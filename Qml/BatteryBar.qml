import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item {
    y: gui.bY
    width: gui.bWidth
    height: gui.bHeight

    property bool generatorMode: false // false = battery mode

    Connections {
        target: powerSourcesCtrl
        function onShowBatteryRestTime(minutes, percent) {
            var time = qsTr("TIME: %1h%2min").arg(Math.floor(minutes/60)).arg((minutes%60)<10 ? "0"+minutes%60 : minutes%60) + infoBarCtrl.emptyString
            estimatedTimeText.text = time
            batteryModeProgressBar.value = percent
            batteryModeProgressBar.text = percent + "%"
            generatorModeProgressBar.value = percent
            generatorModeProgressBar.text = time
        }
        function onShowSilentWatchTimes(timeList) {
            batteryTimeBar.silentWatchTimes = timeList
        }
        function onShowPredictedTime(timeList) {
            let timeArray = Array.from(timeList)
            batteryTimeBar.timeModel = timeArray
        }
    }

    Component.onCompleted: setMode(false, false)

    // set generator mode if at least one generator is running
    function setMode(a, b) {
        generatorMode = a||b
        if(generatorMode) {
            modeDesc.text = qsTr("DESC: Predicted Charge Time") + infoBarCtrl.emptyString
        }
        else {
            modeDesc.text = qsTr("DESC: SoC") + infoBarCtrl.emptyString
        }
    }

    Rectangle {
        anchors.fill: parent; radius: gui.mainItemRadius; color: gui.bBgColor
        Rectangle { width: parent.radius; height: parent.height; color: parent.color }
    }

    Column {
        x: 30/gui.scale
        width: parent.width - x

        Row {
            height: 60/gui.scale
            width: parent.width
            DefaultText {
                text: qsTr("TITLE: Battery") + infoBarCtrl.emptyString
                width: 170/gui.scale
                height: 60/gui.scale
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: gui.bTitleTextSize
                font.bold: true
            }
            Rectangle { width: 1; height: 60/gui.scale; color: gui.splitLineColor; visible: generatorMode }
            BatteryProgressBar { id: generatorModeProgressBar; height: gui.bProgressBarHeight/2; visible: generatorMode }
        }

        DefaultText {
            text: qsTr("DESC: Estimeted time left: ") + infoBarCtrl.emptyString
            x: 170/gui.scale
            height: 50/gui.scale
            leftPadding: 5
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: gui.bInfoTextSize
            visible: !generatorMode
            DefaultText {
                id: estimatedTimeText
                anchors.left: parent.right
                height: 50/gui.scale
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: gui.bInfoTextSize
                font.bold: true
            }
        }

        Item {
            width: 170/gui.scale
            height: 50/gui.scale
            visible: generatorMode
        }

        Row {
            height: 120/gui.scale
            DefaultText {
                id: modeDesc
                width: 170/gui.scale
                height: parent.height
                color: gui.mainTextColor
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: generatorMode ? 22/gui.scale : gui.bInfoTextSize
                font.bold: true
                font.family: gui.mainFont
                rightPadding: 10
                wrapMode: Text.WordWrap
            }
            Rectangle { width: 1; height: 120/gui.scale; color: gui.splitLineColor }
            BatteryProgressBar { id: batteryModeProgressBar; visible: !generatorMode }
            BatteryTimeBar { id: batteryTimeBar; visible: generatorMode }
        }

    }
}
