import QtQuick 2.4
import QtQuick.Controls 2.15
import QtQuick.Extras 1.4

Item {


    ToggleButton {
        id: animationBtn
        x: 15
        y: 15
        text: qsTr("Animation of values")
        onCheckedChanged: {
            simulatorCtrl.setAnimated(checked)
        }
    }

    Text {
        id: element
        x: 15
        y: 195
        text: qsTr("PowerSources")
        font.bold: true
        font.pixelSize: 18
    }

    Button {
        id: startMainGeneratorBtn
        x: 15
        y: 220
        text: qsTr("Start MainGenerator")
        onClicked: {
            simulatorCtrl.startMainGenerator()
        }
    }

    Button {
        id: stopMainGeneratorBtn
        x: 15
        y: 270
        text: qsTr("Stop MainGenerator")
        onClicked: {
            simulatorCtrl.stopMainGenerator()
        }
    }

    Button {
        id: startAuxPowerUnitBtn
        x: 220
        y: 220
        text: qsTr("Start AuxPowerUnit")
        onClicked: {
            simulatorCtrl.startApuGenerator()
        }
    }

    Button {
        id: stopAuxPowerUnitBtn
        x: 220
        y: 270
        text: qsTr("Stop AuxPowerUnit")
        onClicked: {
            simulatorCtrl.stopApuGenerator()
        }
    }

    Text {
        x: 15
        y: 350
        text: qsTr("Battery SoC in %")
        font.bold: true
        font.pixelSize: 16
    }

    SpinBox {
        id: batterySoC
        x: 15
        y: 380
        from: 0
        value: 80
        to: 100
        stepSize: 5
        onValueModified: {
            simulatorCtrl.setBatterySoC(value)
        }
    }

}
