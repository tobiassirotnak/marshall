import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item {
    id: control
    y: (parent.height-height)/2
    width: gui.bProgressBarWidth
    height: gui.bProgressBarHeight

    property alias timeModel: rep.model
    property var silentWatchTimes: []

    Row {
        Repeater {
            id: rep
            Row {
                height: control.height
                Rectangle {
                    width: control.width/4
                    height: parent.height
                    color: modelData ? gui.bProgressBarBgColor : gui.bProgressBarActiveColor
                    border.color: gui.bProgressBarBgColor
                    border.width: 2
                    DefaultText {
                        anchors.centerIn: parent
                        text: (Math.floor(modelData/60)) + ":" + ((modelData%60)<10 ? "0"+modelData%60 : modelData%60)
                        color: gui.mainTextColor
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: gui.bInfoTextSize
                        visible: modelData
                    }
                    DefaultText {
                        width: parent.width
                        anchors.bottom: parent.top
                        text: silentWatchTimes[index]/60 + qsTr("TIME: H") + infoBarCtrl.emptyString
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: gui.bInfoTextSize
                    }
                }
                Rectangle { width: 1; height: parent.height+20/gui.scale; y: -20/gui.scale; color: gui.bSplitTimeLineColor }
            }
        }
    }
    DefaultText {
        text: qsTr("DESC: Silent watch") + infoBarCtrl.emptyString
        x: -width
        y: -53/gui.scale
        width: 170/gui.scale
        height: 50/gui.scale
        rightPadding: 5
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignBottom
        font.pixelSize: gui.bTitleTextSize
        visible: generatorMode
    }
}
