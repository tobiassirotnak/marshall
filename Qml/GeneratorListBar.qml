import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import Generator.State 1.0

Item {
    id: control
    y: gui.gY
    width: gui.gWidth
    height: gui.gHeight

    signal generatorStatesChanged(bool generatorRunning, bool apuRunning)

    Rectangle {
        anchors.fill: parent; radius: gui.mainItemRadius; color: gui.gBgColor;
        Rectangle { width: parent.radius; height: parent.height; color: parent.color }
    }

    Column {
        x: 30/gui.scale
        width: parent.width - x
        DefaultText {
            text: qsTr("TITLE: Generator") + infoBarCtrl.emptyString
            width: parent.width
            height: 60/gui.scale
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: gui.gTitleTextSize
            font.bold: true
        }
        GeneratorBar {
            id: meg
            isMainGen: true
            text: qsTr("DESC: Main engine generator") + infoBarCtrl.emptyString
            state: "Shutdown"
            onStateChanged: control.generatorStatesChanged( // show the right battery bar
                                meg.state == "Running" || meg.state == "AskForShuttingDown" || meg.state == "WaitForShuttingDown",
                                apu.state == "Running" || apu.state == "AskForShuttingDown" || apu.state == "WaitForShuttingDown")
        }
        Rectangle { width: 1; height: 5; color: "transparent" }
        GeneratorBar {
            id: apu
            isMainGen: false
            text: qsTr("DESC: Auxiliary power unit") + infoBarCtrl.emptyString
            state: "Shutdown"
            onStateChanged: control.generatorStatesChanged( // show the right battery bar
                                meg.state == "Running" || meg.state == "AskForShuttingDown" || meg.state == "WaitForShuttingDown",
                                apu.state == "Running" || apu.state == "AskForShuttingDown" || apu.state == "WaitForShuttingDown")
        }
    }
}
