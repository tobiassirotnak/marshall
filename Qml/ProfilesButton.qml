import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: control
    width: gui.pButtonWidth
    height: gui.pButtonHeight
    // don't activate the button when finger was moving away from button (activate only when finger comes back)
    onHoveredChanged: if(!hovered && pressed) scale = 1

    property string timeDesc // text for 2nd button row

    signal timeout()

    Component.onCompleted: if(state === "") state = "InActive"

    contentItem: Column {
        width: parent.width
        height: parent.height
        DefaultText {
            width: parent.width
            height: parent.height / (timeText.visible ? 2 : 1)
            text: control.state === "Waiting" ? qsTr("Button: OK") : control.text
            font.pixelSize: gui.pButtonTextSize
            font.bold: true
            color: gui.pButtonTextColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
        DefaultText {
            id: timeText
            width: parent.width
            height: parent.height / 2
            text: control.timeDesc
            font.pixelSize: gui.pButtonTextSize
            font.bold: false
            color: gui.pButtonTextColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            visible: timeDesc.length > 0 && control.state !== "Waiting"
        }
    }

    background: Rectangle {
        id: bg
        implicitWidth: gui.pButtonWidth
        implicitHeight: gui.pButtonHeight
        border.color: gui.pButtonBorderColor
        border.width: 1
        radius: gui.mainItemRadius
    }

    onPressed: { scale = 0.95; gui.playPress() }
    onReleased: {
        scale = 1;
        gui.playRelease()
        if(state === "InActive")
            waitingTimer.start()
    }

    Timer {
        id: waitingTimer
        interval: gui.pWaitingTime; running: false; repeat: false; triggeredOnStart: false
        onTriggered: control.timeout()
    }

    onStateChanged: if(state !== "Waiting") waitingTimer.stop()

    // zoom aniamtion for press and release the button
    Behavior on scale {
        NumberAnimation {
            duration: 100
            easing.type: Easing.InOutQuad
        }
    }

    states: [
        State {
            name: "InActive"
            PropertyChanges { target: bg; color: gui.pButtonBgColor }
        },
        State {
            name: "Active"
            PropertyChanges { target: bg; color: gui.pButtonActiveBgColor }
        },
        State {
            name: "Waiting"
            PropertyChanges { target: bg; color: gui.pButtonWaitingBgColor }
        },
        State {
            name: "Disabled"
            PropertyChanges { target: bg; color: gui.pButtonDisabledBgColor }
        }
    ]
}
