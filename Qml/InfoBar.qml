import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item {
    id: control
    y: -height
    width: parent.width
    height: gui.ibHeight

    Connections {
        target: infoBarCtrl
        function onShow(text, duration) {
            console.log("show: " + text)
            info.text = text
            if(infoTimer.running)
                infoTimer.stop()
            if(duration > 0) {
                infoTimer.interval = duration*1000
                infoTimer.running = true
            }
        }
    }

    Timer {
        id: infoTimer
        interval: 1000; running: false; repeat: false; triggeredOnStart: false
        onTriggered: info.text = ""
    }

    Rectangle {
        anchors.fill: parent; color: gui.ibBgColor; radius: gui.mainItemRadius
        Rectangle {width: parent.width; height: parent.radius; color: parent.color}
    }


    states: State {
            name: "change_position";
            PropertyChanges { target: control; y: 0 }
        }

    SequentialAnimation {
        id: startup_animation
        PauseAnimation { duration: gui.adDuration }
        PropertyAnimation { target: control; property: "y"; to: 0; duration: gui.startAppAnimationDuration; easing.type: Easing.OutSine }
        onFinished: control.state = "change_position"
    }

    // Animation at the beginning
    Component.onCompleted: {
        startup_animation.start()
    }


    // Select language by clicking on the flag
    Drawer {
        id: flagDrawer
        width: 400/gui.scale
        height: gui.ibHeight

        Rectangle { anchors.fill: parent; color: gui.ibBgColor }

        Row {
            spacing: 10/scale
            anchors.centerIn: parent
            Image { source: "qrc:/Images/EN.png"; width: gui.ibFlagWidth; fillMode: Image.PreserveAspectFit
                Rectangle { anchors.fill: parent; border.width: 1; border.color: gui.ibFlagBorderColor; color: "transparent" }
                MouseArea { anchors.fill: parent; onClicked: { flagDrawer.close(); infoBarCtrl.setLanguage("en") } }
            }
            Image { source: "qrc:/Images/DE.png"; width: gui.ibFlagWidth; fillMode: Image.PreserveAspectFit
                Rectangle { anchors.fill: parent; border.width: 1; border.color: gui.ibFlagBorderColor; color: "transparent" }
                MouseArea { anchors.fill: parent; onClicked: { flagDrawer.close(); infoBarCtrl.setLanguage("de"); flag.source = parent.source } }
            }
            Image { source: "qrc:/Images/FR.png"; width: gui.ibFlagWidth; fillMode: Image.PreserveAspectFit
                Rectangle { anchors.fill: parent; border.width: 1; border.color: gui.ibFlagBorderColor; color: "transparent" }
                MouseArea { anchors.fill: parent; onClicked: { flagDrawer.close(); infoBarCtrl.setLanguage("fr"); flag.source = parent.source } }
            }
            Image { source: "qrc:/Images/IT.png"; width: gui.ibFlagWidth; fillMode: Image.PreserveAspectFit
                Rectangle { anchors.fill: parent; border.width: 1; border.color: gui.ibFlagBorderColor; color: "transparent" }
                MouseArea { anchors.fill: parent; onClicked: { flagDrawer.close(); infoBarCtrl.setLanguage("it"); flag.source = parent.source } }
            }
        }
    }

    Row {
        height: parent.height
        width: parent.width

        Item {
            width: gui.ibFlagPlaceWidth
            height: parent.height

            Image {
                id: flag
                anchors.centerIn: parent
                source: "qrc:/Images/EN.png"
                width: gui.ibFlagWidth
                fillMode: Image.PreserveAspectFit
                Rectangle { anchors.fill: parent; border.width: 1; border.color: gui.ibFlagBorderColor; color: "transparent" }
            }
            MouseArea { anchors.fill: parent; onClicked: flagDrawer.open() }
        }


        DefaultText {
            text: qsTr("TITLE: Dashboard") + infoBarCtrl.emptyString
            width: gui.ibTitleWidth
            height: parent.height
            color: gui.ibTitleColor
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: gui.ibTitleTextSize
            font.bold: true
        }
        DefaultText {
            id: info
            width: gui.ibInfoWidth
            height: parent.height
            color: gui.ibInfoColor
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: gui.ibInfoTextSize
            clip: true
        }
        Item { // To show current date / time
            width: gui.ibDateTimeWidth
            height: parent.height
            Timer {
                interval: 1000; running: true; repeat: true; triggeredOnStart: true
                onTriggered: {
                    var currentDate = new Date()
                    time.text = Qt.formatTime(currentDate, gui.ibTimeFormat)
                    date.text = Qt.formatDate(currentDate, gui.ibDateFormat)
                }
            }
            Column {
                width: parent.width
                height: parent.height
                DefaultText {
                    id: time
                    width: parent.width
                    height: parent.height/2
                    color: gui.ibDateTimeColor
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: gui.ibTimeTextSize
                    font.bold: true
                }
                DefaultText {
                    id: date
                    width: parent.width
                    height: parent.height/2
                    color: gui.ibDateTimeColor
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: gui.ibDateTextSize
                    font.bold: false
                }
            }
        }
    }
}
