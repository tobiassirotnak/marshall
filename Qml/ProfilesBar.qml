import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item {
    id: control
    y: gui.mainHeight
    width: gui.pWidth
    height: gui.pHeight

    property string lastActiveState: "" // remember last active state, in case to return back, it waiting timer fires

    Connections {
        target: deviceListCtrl
        function onSetProfile(state) {
            console.log("onSetProfile: " + state)
            if(state === 1) {
                control.state = "Active1"; deviceListCtrl.loadDevices(gui.pProfileCustomNr)
            }
            else if(state === 2) {
                control.state = "Active2"; deviceListCtrl.loadDevices(gui.pProfileStandardNr)
            }
            else if(state === 3) {
                control.state = "Active3"; deviceListCtrl.loadDevices(gui.pProfileSilentWatchNr)
            }
            else if(state === 4) {
                control.state = "Active4"; deviceListCtrl.loadDevices(gui.pProfileProfile3Nr)
            }
            else {
                control.state = "Active1"; // never shoud goes here
            }
        }
    }


    Rectangle {
        anchors.fill: parent
        radius: gui.mainItemRadius
        color: gui.pBgColor
        Rectangle { width: parent.radius; height: parent.height; color: parent.color }
        Rectangle { width: parent.width; height: parent.radius; color: parent.color; anchors.bottom: parent.bottom }
    }




    SequentialAnimation {
        id: startup_animation
        PauseAnimation { duration: gui.adDuration }
        PropertyAnimation { target: control; property: "y"; to: gui.pY; duration: gui.startAppAnimationDuration; easing.type: Easing.OutSine }
        onFinished: control.state = "change_position"
    }

    // Animation at the beginning
    Component.onCompleted: {
        startup_animation.start()
    }



    Column {
        x: 30/gui.scale
        width: parent.width - x
        height: parent.height
        DefaultText {
            text: qsTr("TITLE: Profiles") + infoBarCtrl.emptyString
            width: parent.width
            height: 60/gui.scale
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: gui.pTitleTextSize
            font.bold: true
        }
        Grid {
            id: grid
            columns: 2
            rowSpacing: gui.pGridSpacing
            columnSpacing: gui.pGridSpacing
            ProfilesButton { // this button should be not selectable. It shows only status, that no other profile is selected
                id: b1
                text: qsTr("BUTTON: Custom") + infoBarCtrl.emptyString
                enabled: false
                /*onClicked: {
                    if(state === "Active" || state === "Disabled")
                        return
                    if(state === "InActive") {
                        lastActiveState = control.state
                        control.state = "Waiting1"
                    }
                    else if(state === "Waiting") {
                        control.state = "Active1"
                        deviceListCtrl.loadDevices(gui.pProfileCustomNr)
                    }
                }
                onTimeout: control.state = lastActiveState*/
            }
            ProfilesButton {
                id: b2
                text: qsTr("BUTTON: Default") + infoBarCtrl.emptyString
                //state: "Active" // set default profile
                onClicked: {
                    if(state === "Active" || state === "Disabled")
                        return
                    if(state === "InActive") {
                        lastActiveState = control.state
                        control.state = "Waiting2"
                    }
                    else if(state === "Waiting") {
                        control.state = "Active2"
                        deviceListCtrl.loadDevices(gui.pProfileStandardNr)
                    }
                }
                onTimeout: control.state = lastActiveState
            }
            ProfilesButton {
                id: b3
                text: qsTr("BUTTON: Profile1") + infoBarCtrl.emptyString
                timeDesc: qsTr("BUTTON TIME: %1h%2min").arg(1).arg(15) + infoBarCtrl.emptyString
                onClicked: {
                    if(state === "Active" || state === "Disabled")
                        return
                    if(state === "InActive") {
                        lastActiveState = control.state
                        control.state = "Waiting3"
                    }
                    else if(state === "Waiting") {
                        control.state = "Active3"
                        deviceListCtrl.loadDevices(gui.pProfileSilentWatchNr)
                    }
                }
                onTimeout: control.state = lastActiveState
            }
            ProfilesButton {
                id: b4
                text: qsTr("BUTTON: Profile2") + infoBarCtrl.emptyString
                timeDesc: qsTr("BUTTON TIME: %1h%2min").arg(1).arg(15) + infoBarCtrl.emptyString
                onClicked: {
                    if(state === "Active" || state === "Disabled")
                        return
                    if(state === "InActive") {
                        lastActiveState = control.state
                        control.state = "Waiting4"
                    }
                    else if(state === "Waiting") {
                        control.state = "Active4"
                        deviceListCtrl.loadDevices(gui.pProfileProfile3Nr)
                    }
                }
                onTimeout: control.state = lastActiveState
            }
        }
    }

    states: [
        State {
            name: "Active1"
            extend: "change_position"
            PropertyChanges { target: b1; state: "Active" }
            PropertyChanges { target: b2; state: "InActive" }
            PropertyChanges { target: b3; state: "InActive" }
            PropertyChanges { target: b4; state: "InActive" }
        },
        State {
            name: "Active2"
            extend: "change_position"
            PropertyChanges { target: b1; state: "InActive" }
            PropertyChanges { target: b2; state: "Active" }
            PropertyChanges { target: b3; state: "InActive" }
            PropertyChanges { target: b4; state: "InActive" }
        },
        State {
            name: "Active3"
            extend: "change_position"
            PropertyChanges { target: b1; state: "InActive" }
            PropertyChanges { target: b2; state: "InActive" }
            PropertyChanges { target: b3; state: "Active" }
            PropertyChanges { target: b4; state: "InActive" }
        },
        State {
            name: "Active4"
            extend: "change_position"
            PropertyChanges { target: b1; state: "InActive" }
            PropertyChanges { target: b2; state: "InActive" }
            PropertyChanges { target: b3; state: "InActive" }
            PropertyChanges { target: b4; state: "Active" }
        },
        State {
            name: "Waiting1"
            extend: "change_position"
            PropertyChanges { target: b1; state: "Waiting" }
            PropertyChanges { target: b2; state: "Disabled" }
            PropertyChanges { target: b3; state: "Disabled" }
            PropertyChanges { target: b4; state: "Disabled" }
        },
        State {
            name: "Waiting2"
            extend: "change_position"
            PropertyChanges { target: b1; state: "Disabled" }
            PropertyChanges { target: b2; state: "Waiting" }
            PropertyChanges { target: b3; state: "Disabled" }
            PropertyChanges { target: b4; state: "Disabled" }
        },
        State {
            name: "Waiting3"
            extend: "change_position"
            PropertyChanges { target: b1; state: "Disabled" }
            PropertyChanges { target: b2; state: "Disabled" }
            PropertyChanges { target: b3; state: "Waiting" }
            PropertyChanges { target: b4; state: "Disabled" }
        },
        State {
            name: "Waiting4"
            extend: "change_position"
            PropertyChanges { target: b1; state: "Disabled" }
            PropertyChanges { target: b2; state: "Disabled" }
            PropertyChanges { target: b3; state: "Disabled" }
            PropertyChanges { target: b4; state: "Waiting" }
        },
        State {
            name: "change_position";
            PropertyChanges { target: control; y: !startup_animation.running ? gui.pY : gui.mainHeight }
        }
    ]
}
