import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: control
    width: gui.dButtonWidth
    height: parent.height - 10/gui.scale
    anchors.verticalCenter: parent.verticalCenter
    // don't activate the button when finger was moving away from button (activate only when finger comes back)
    onHoveredChanged: if(!hovered && pressed) scale = 1

    property string timeDesc // text for 2nd button row

    contentItem: Column {
        width: parent.width
        height: parent.height
        DefaultText {
            text: control.text
            width: parent.width
            height: parent.height / (timeDesc.length > 0 ? 2 : 1)
            font.pixelSize: gui.dButtonTextSize
            font.bold: true
            color: gui.dButtonTextColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
        DefaultText {
            width: parent.width
            height: parent.height / 2
            text: control.timeDesc
            font.pixelSize: gui.dButtonTextSize
            font.bold: false
            color: gui.dButtonTextColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            visible: timeDesc.length > 0
        }
    }

    background: Rectangle {
        id: bg
        implicitWidth: parent.width
        implicitHeight: parent.height
        radius: gui.mainItemRadius
    }


    onPressed: { scale = 0.95; gui.playPress() }
    onReleased: { scale = 1; gui.playRelease() }

    // zoom aniamtion for press and release the button
    Behavior on scale {
        NumberAnimation {
            duration: 100
            easing.type: Easing.InOutQuad
        }
    }

    states: [
        State {
            name: "Turn_Off"
            PropertyChanges { target: control; text: qsTr("BUTTON: Turn-off") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonActiveBgColor }
        },
        State {
            name: "Turn_On"
            PropertyChanges { target: control; text: qsTr("BUTTON: Turn-on") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonBgColor }
        },
        State {
            name: "Add_Source"
            PropertyChanges { target: control; text: qsTr("BUTTON: Add. Source") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonInfoBgColor }
        },
        State {
            name: "Override"
            PropertyChanges { target: control; text: qsTr("BUTTON: Override") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonAttentionBgColor }
        },
        State {
            name: "Reset"
            PropertyChanges { target: control; text: qsTr("BUTTON: Reset") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonAttentionBgColor }
        },
        State {
            name: "Action_Required"
            PropertyChanges { target: control; text: qsTr("BUTTON: Action Required") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonAttentionBgColor }
        }
    ]
}
