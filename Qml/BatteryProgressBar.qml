import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

ProgressBar {
    id: control
    y: (parent.height-height)/2
    width: gui.bProgressBarWidth
    height: gui.bProgressBarHeight
    from: 0
    to: 100
    value: 0
    padding: 2

    property string text: ""

    background: Rectangle {
        implicitWidth: control.width
        implicitHeight: control.height
        color: gui.bProgressBarBgColor
        radius: 3
    }

    contentItem: Item {
        implicitWidth: control.width
        implicitHeight: control.height

        Rectangle {
            width: control.visualPosition * parent.width
            height: parent.height
            radius: 2
            color: gui.bProgressBarActiveColor

            DefaultText {
                anchors.centerIn: parent
                text: control.text
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: gui.bInfoTextSize
                visible: parent.width > width
                leftPadding: 10
                rightPadding: 10
            }

            DefaultText {
                anchors.left: parent.right
                anchors.verticalCenter: parent.verticalCenter
                text: control.text
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: gui.bInfoTextSize
                visible: parent.width <= width
                leftPadding: 10
                rightPadding: 10
            }
        }
    }

    // Animation used just for test
//    SequentialAnimation {
//        running: true
//        loops: Animation.Infinite
//        NumberAnimation { target: control; property: "value"; to: 0; duration: 20000 }
//        NumberAnimation { target: control; property: "value"; to: 1; duration: 500 }
//    }
}
