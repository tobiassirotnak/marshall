import QtQuick 2.12
import QtMultimedia 5.15

Item {
    // Main properties
    property int width1920: 1920                    // All the positions and sizes will be calculated to this initial width
    property int height1200: 1200                   // All the positions and sizes will be calculated to this initial height
    property real ratio: width1920/height1200       // Ratio width/height = 1.6 when screen size is 1920x1200
    property int initialWidth: _screenWidth         // Initial width should be 1920
    property int initialHeight: _screenHeight       // Initial height should be 1200
    property real mainWidth: initialWidth           // The current width of the main item
    property real mainHeight: initialHeight         // The current height of the main item
    property color windowColor: "black"             // This color will be visible at the borders, when ratio is not 1.6
    property color backgroundColor: "#1d4c77"       // This color will be visible between main items
    property color mainItemBgColor: "#181745"       // Background color of main items
    property real scale: width1920/mainWidth        // Scale all the positions and sizes according to current app size
    property real mainItemRadius: 30/scale          // Radius of main item rectangles
    property color mainTextColor: "white"           // Main text color
    property color disabledTextColor: "gray"        // Text color for disabled state
    property color splitLineColor: "white"          // Vertical split-line color
    property int adDuration: 1000                   // Duration of showing ad at the beginning
    property int startAppAnimationDuration: 1000    // Duration of animation between ad and control items
    property string mainFont: "Arial"               // Main font of the used text

    // InfoBar properties
    property real ibHeight: 90/scale                // Height
    property color ibBgColor: mainItemBgColor       // Background color
    property real ibFlagPlaceWidth: 120/scale       // Width of place for flag
    property real ibFlagWidth: 60/scale             // Width for flag
    property color ibFlagBorderColor: "darkGray"    // Border color around a flag
    property real ibTitleWidth: 280/scale           // Max title width
    property color ibTitleColor: mainTextColor      // Title background color
    property real ibTitleTextSize: 40/scale         // Title text pixel size
    property real ibInfoWidth: 1280/scale           // Max info width
    property color ibInfoColor: mainTextColor       // Info background color
    property real ibInfoTextSize: 36/scale          // Info text pixel size
    property real ibDateTimeWidth: 240/scale        // Max width of date/time
    property color ibDateTimeColor: mainTextColor   // Background color of date/time
    property real ibTimeTextSize: 28/scale          // Time text pixel size
    property real ibDateTextSize: 28/scale          // Date text pixel size
    property string ibTimeFormat: "hh:mm"           // Time format, see Qt.formatTime
    property string ibDateFormat: "dd.MM.yyyy"      // Date format, see Qt.formatDate

    // PowerSources properties
    property real psY: 120/scale                    // Y position
    property real psWidth: 800/scale                // Width
    property real psHeight: 670/scale               // Height
    property color psBgColor: mainItemBgColor       // Background color
    property real psTitleTextSize: 28/scale         // Title text pixel size

    // Battery properties
    property real bY: 60/scale                      // Y position
    property real bWidth: 780/scale                 // Width
    property real bHeight: 240/scale                // Height
    property color bBgColor: "#212164"              // Background color
    property real bTitleTextSize: 24/scale          // Title text pixel size
    property real bInfoTextSize: 34/scale           // Info text pixel size
    property real bProgressBarWidth: 550/scale      // Width of progress bar
    property real bProgressBarHeight: 80/scale      // Height of progress bar
    property color bProgressBarBgColor: "black"     // Background color of progress bar
    property color bProgressBarActiveColor: "green" // Actice color of progress bar
    property color bSplitTimeLineColor: "lightGray" // Vertical split-line color on TimeBar

    // Generator properties
    property real gY: 320/scale                     // Y position
    property real gWidth: 780/scale                 // Width
    property real gHeight: 330/scale                // Height
    property color gBgColor: "#212164"              // Background color
    property real gTitleTextSize: 24/scale          // Title text pixel size
    property real gInfoTextSize: 36/scale           // Info text pixel size
    property real gButtonWidth: 220/scale           // Button width
    property real gButtonHeight: 120/scale          // Button height
    property real gButtonTextSize: 36/scale         // Button text pixel size
    property color gButtonTextColor: "white"        // Button text color
    property color gButtonBgColor: "darkOrange"     // Button non-active background color
    property color gButtonActiveBgColor: "green"    // Button active background color
    property color gButtonDisabledBgColor: "gray"   // Button disabled background color
    property color gButtonOkBgColor: "green"        // Button OK background color
    property color gButtonCancelBgColor: "white"    // Button Cancel background color

    // Profiles properties
    property real pY: 820/scale                     // Y position
    property real pWidth: 800/scale                 // Width
    property real pHeight: 380/scale                // Height
    property color pBgColor: mainItemBgColor        // Background color
    property real pTitleTextSize: 28/scale          // Title text pixel size
    property real pButtonWidth: 360/scale           // Button width
    property real pButtonHeight: 120/scale          // Button height
    property real pButtonTextSize: 36/scale         // Button text pixel size
    property real pGridSpacing: 15/scale            // Buttons grid spacing size
    property color pButtonTextColor: "white"        // Button text color
    property color pButtonBorderColor: "gray"       // Button border color
    property color pButtonBgColor: "transparent"    // Button non-active background color
    property color pButtonActiveBgColor: "#1d4c77"  // Button active background color
    property color pButtonWaitingBgColor: "darkOrange"  // Button waiting background color
    property color pButtonDisabledBgColor: "gray"   // Button disabled background color
    property int pProfileCustomNr: 1                // Number of Profile "Custom"
    property int pProfileStandardNr: 2              // Number of Profile "Standard"
    property int pProfileSilentWatchNr: 3           // Number of Profile "SilentWatch"
    property int pProfileProfile3Nr: 4              // Number of Profile "Profile3"
    property int pWaitingTime: 3000                 // Time to press OK button again to select the profile

    // Device list properties
    property real dlX: 830/scale                    // Y position
    property real dlY: 120/scale                    // Y position
    property real dlWidth: 1090/scale               // Width
    property real dlHeight: 1080/scale              // Height
    property color dlBgColor: mainItemBgColor       // Background color

    // Device properties
    property real dTextWidth: 230/scale             // Text width
    property real dPowerBarWidth: 530/scale         // Powerbar width
    property real dPowerBarOffWidth: 10/scale       // Powerbar width in state OFF
    //property real dPowerBarHeight: 80/scale       // Powerbar height (half of powerbar height)
    property color dPowerBarColor: "#ab26c7"        // Powerbar color
    property real dAveragePowerBarHeight: 20/scale  // Average powerbar height
    property color dAveragePowerBarColor: "#7030a0" // Average powerbar color
    property real dButtonWidth: 230/scale           // Button width
    property real dSpaceBeforeButton: 20/scale      // Space between powerbar and button
    property real dTitleTextSize: 36/scale          // Title text pixel size
    property real dPowerbarTextSize: 28/scale       // Powerbar text pixel size
    property real dInfoTextSize: 28/scale           // Info text pixel size
    property color dTitleColor: mainTextColor       // Title background color
    property real dButtonTextSize: 32/scale         // Button text pixel size
    property color dButtonTextColor: "white"        // Button text color
    property color dButtonBgColor: "darkOrange"     // Button non-active background color
    property color dButtonActiveBgColor: "green"    // Button active background color
    property color dButtonAttentionBgColor: "red"   // Button attention background color
    property color dButtonInfoBgColor: "#5b9bd5"    // Button info background color used for Add.Source
    property color dButtonWaitingBgColor: "gray"    // Button background color used for waiting of main state
    property color dButtonCancelBgColor: "white"    // Button Cancel background color

    // Sound effects
    SoundEffect { id: pressSound; source: "qrc:/Sounds/Press.wav" }
    SoundEffect { id: releaseSound; source: "qrc:/Sounds/Release.wav" }
    function playPress() { pressSound.play() }
    function playRelease() { releaseSound.play() }

    // Voices
    SoundEffect { id: actionRequiredSound; source: "qrc:/Sounds/ActionRequired_"+infoBarCtrl.language+".wav" }
    function playActionRequired() { actionRequiredSound.play() }
}
