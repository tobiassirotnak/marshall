import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import Generator.State 1.0

Item {
    id: control
    height: 120/gui.scale
    width: parent.width

    property bool isMainGen: false // flag, if this generator in Main-Engine Generator (true), or APU (false)
    property alias text: name.text

    Connections {
        target: powerSourcesCtrl
        function onSetMainGeneratorState(state) {
            if(!isMainGen)
                return
            console.log("onSetMainGeneratorState: " + state)
            if(state === GState.SHUTDOWN) control.state = "Shutdown"
            else if(state === GState.ASK_FOR_RUNNING) control.state = "AskForRunning"
            else if(state === GState.WAIT_FOR_RUNNING) control.state = "WaitForRunning"
            else if(state === GState.RUNNING) control.state = "Running"
            else if(state === GState.ASK_FOR_SHUTTING_DOWN) control.state = "AskForShuttingDown"
            else if(state === GState.WAIT_FOR_SHUTTING_DOWN) control.state = "WaitForShuttingDown"
        }
        function onSetApuGeneratorState(state) {
            if(isMainGen)
                return
            console.log("onSetApuGeneratorState: " + state)
            if(state === GState.SHUTDOWN) control.state = "Shutdown"
            else if(state === GState.ASK_FOR_RUNNING) control.state = "AskForRunning"
            else if(state === GState.WAIT_FOR_RUNNING) control.state = "WaitForRunning"
            else if(state === GState.RUNNING) control.state = "Running"
            else if(state === GState.ASK_FOR_SHUTTING_DOWN) control.state = "AskForShuttingDown"
            else if(state === GState.WAIT_FOR_SHUTTING_DOWN) control.state = "WaitForShuttingDown"
        }
    }

    Row {
        id: row
        height: parent.height

        DefaultText {
            id: name
            width: 260/gui.scale
            height: parent.height
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: gui.gInfoTextSize
            wrapMode: Text.WordWrap
            rightPadding: 10
            leftPadding: 10
        }
        Rectangle { width: 1; height: parent.height; color: gui.splitLineColor }
        DefaultText {
            text: button2.state==GeneratorButton.XState.Shutdown ? qsTr("STATE: Off") + infoBarCtrl.emptyString : button2.state==GeneratorButton.XState.Running ? qsTr("STATE: Running") + infoBarCtrl.emptyString : text
            width: 250/gui.scale
            height: parent.height
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: gui.gInfoTextSize
            font.bold: true
            wrapMode: Text.WordWrap
            rightPadding: 10
            leftPadding: 10
            visible: !button1.visible
        }
        Item { width: 10/gui.scale; height: 1; visible: button1.visible }
        GeneratorButton {
            id: button1
            state: GeneratorButton.XState.Shutdown
            onReleased: {
                if(state == GeneratorButton.XState.OkStarting)
                {
                    powerSourcesCtrl.tryToChangeState(isMainGen, GeneratorButton.XState.OkStarting)
                }
                else if(state == GeneratorButton.XState.OkStopping)
                {
                    powerSourcesCtrl.tryToChangeState(isMainGen, GeneratorButton.XState.OkStopping)
                }
            }
        }
        Item { width: 20/gui.scale; height: 1; visible: button1.visible }
        GeneratorButton {
            id: button2
            state: GeneratorButton.XState.Shutdown
            onReleased: {
                if(state == GeneratorButton.XState.Shutdown) {
                    powerSourcesCtrl.tryToChangeState(isMainGen, GeneratorButton.XState.Running)
                }
                else if(state == GeneratorButton.XState.Running) {
                    powerSourcesCtrl.tryToChangeState(isMainGen, GeneratorButton.XState.Shutdown)
                }
                else if(state == GeneratorButton.XState.Cancel) {
                    powerSourcesCtrl.tryToChangeState(isMainGen, GeneratorButton.XState.Cancel)
                }                
            }
        }
    }

    states: [
        State {
            name: "Shutdown"
            PropertyChanges { target: button1; visible: false }
            PropertyChanges { target: button2; visible: true; state: GeneratorButton.XState.Shutdown }
        },
        State {
            name: "AskForRunning"
            PropertyChanges { target: button1; visible: true; state: GeneratorButton.XState.OkStarting }
            PropertyChanges { target: button2; visible: true; state: GeneratorButton.XState.Cancel }
        },
        State {
            name: "WaitForRunning"
            PropertyChanges { target: button1; visible: true; state: GeneratorButton.XState.WaitingForStarting }
            PropertyChanges { target: button2; visible: false }
        },
        State {
            name: "Running"
            PropertyChanges { target: button1; visible: false }
            PropertyChanges { target: button2; visible: true; state: GeneratorButton.XState.Running }
        },
        State {
            name: "AskForShuttingDown"
            PropertyChanges { target: button1; visible: true; state: GeneratorButton.XState.OkStopping }
            PropertyChanges { target: button2; visible: true; state: GeneratorButton.XState.Cancel }
        },
        State {
            name: "WaitForShuttingDown"
            PropertyChanges { target: button1; visible: true; state: GeneratorButton.XState.WaitingForShutdown }
            PropertyChanges { target: button2; visible: false }
        }
    ]
}
