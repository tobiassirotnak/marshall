import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Window {
    property Style gui: Style{}

    id: mainWindow
    visible: true
    width: gui.initialWidth
    height: gui.initialHeight
    title: qsTr("WINDOW TITLE: Dashboard") + infoBarCtrl.emptyString
    color: gui.windowColor
    // TODO: UNCOMMENT FOR RUNNING ON TABLET IN FULLSCREEN MODE
    //visibility: Window.FullScreen // show window in fullscreen mode

    Timer { // Workaround if Fullscreen is not appearing at the beginning
        interval: 300; running: true; repeat: false; triggeredOnStart: false
        onTriggered: {
            // TODO: UNCOMMENT FOR RUNNING ON TABLET IN FULLSCREEN MODE
            //mainWindow.visibility = Window.FullScreen
        }
    }

    Timer { // show logo and all the other items
        interval: 500; running: true; repeat: false; triggeredOnStart: false
        onTriggered: {
            logo.opacity = 1
            infoBar.visible = true
            powerSourcesBar.visible = true
            profilesBar.visible = true
            deviceListBar.visible = true
        }
    }

    Component.onCompleted: {
        console.log("Screen size: " + gui.initialWidth + " x " + gui.initialHeight)
    }

    // Catch resize of the app window and set the right size by ratio. (Used just for testing of different app size)
    onWidthChanged: {
        gui.mainWidth = (mainWindow.width > gui.ratio*mainWindow.height) ? mainWindow.height*gui.ratio : mainWindow.width
        gui.mainHeight = (mainWindow.width < gui.ratio*mainWindow.height) ? mainWindow.width/gui.ratio : mainWindow.height
    }
    onHeightChanged: {
        gui.mainWidth = (mainWindow.width > gui.ratio*mainWindow.height) ? mainWindow.height*gui.ratio : mainWindow.width
        gui.mainHeight = (mainWindow.width < gui.ratio*mainWindow.height) ? mainWindow.width/gui.ratio : mainWindow.height
    }

    Item {
        id: appWindow
        anchors.centerIn: parent
        width: gui.mainWidth
        height: gui.mainHeight

        Rectangle { anchors.fill: parent; color: gui.backgroundColor }

        Image {
            id: logo
            anchors.centerIn: parent
            source: "qrc:/Images/RuagLogo.png"
            opacity: 0

            // Animation at the beginning
            Component.onCompleted: SequentialAnimation {
                running: true
                PauseAnimation { duration: gui.adDuration }
                ParallelAnimation {
                    PropertyAnimation { target: logo; property: "opacity"; to: 0; duration: gui.startAppAnimationDuration; easing.type: Easing.OutQuint }
                    PropertyAnimation { target: logo; property: "scale"; to: 2; duration: gui.startAppAnimationDuration; easing.type: Easing.OutSine }
                }
            }
        }

        InfoBar { id: infoBar; visible: false }
        PowerSourcesBar { id: powerSourcesBar; visible: false }
        ProfilesBar { id: profilesBar; visible: false }
        DeviceListBar { id: deviceListBar; visible: false }
    }

    Loader {
      anchors.fill: parent
      objectName: "simulatorWindow"
      source: "SimulatorApp.qml"
    }
}
