import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item {
    id: control
    x: -width
    y: gui.psY
    width: gui.psWidth
    height: gui.psHeight

    Rectangle {
        anchors.fill: parent; radius: gui.mainItemRadius; color: gui.psBgColor
        Rectangle { width: parent.radius; height: parent.height; color: parent.color }
    }



    states: State {
            name: "change_position";
            PropertyChanges { target: control; x: 0 }
        }

    SequentialAnimation {
        id: startup_animation
        PauseAnimation { duration: gui.adDuration }
        PropertyAnimation { target: control; property: "x"; to: 0; duration: gui.startAppAnimationDuration; easing.type: Easing.OutSine }
        onFinished: control.state = "change_position"
    }

    // Animation at the beginning
    Component.onCompleted: {
        startup_animation.start()
    }


    DefaultText {
        x: 30/gui.scale
        text: qsTr("TITLE: Power sources") + infoBarCtrl.emptyString
        width: parent.width
        height: 60/gui.scale
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: gui.psTitleTextSize
        font.bold: true
    }

    BatteryBar { Component.onCompleted: generatorListBar.generatorStatesChanged.connect(setMode) }
    GeneratorListBar { id: generatorListBar }
}
