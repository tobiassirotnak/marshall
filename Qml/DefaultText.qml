import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12


Text {
    fontSizeMode: Text.Fit
    color: gui.mainTextColor
    font.family: gui.mainFont
}
