import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.VirtualKeyboard 2.15

Window {
    id: window
    visible: true
    width: 800
    height: 600

    title: qsTr("Simulator")

    Loader {
      anchors.fill: parent
      source: "SimMainWindow.qml"
    }
}
