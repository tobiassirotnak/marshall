import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: control
    width: gui.gButtonWidth
    height: gui.gButtonHeight

    enum XState {
        Shutdown,
        WaitingForStarting,
        WaitingForShutdown,
        Running,
        OkStarting,
        OkStopping,
        Cancel
    }

    contentItem: Column {
        width: parent.width
        height: parent.height
        DefaultText {
            text: control.text
            width: parent.width
            height: parent.height
            font.pixelSize: gui.gButtonTextSize
            font.bold: true
            color: bg.color == "#ffffff" ? "black" : gui.gButtonTextColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
    }

    background: Rectangle {
        id: bg
        implicitWidth: gui.gButtonWidth
        implicitHeight: gui.gButtonHeight
        radius: gui.mainItemRadius
    }

    onPressed: { scale = 0.95; gui.playPress() }
    onReleased: { scale = 1; gui.playRelease() }
    // don't activate the button when finger was moving away from button (activate only when finger comes back)
    onHoveredChanged: if(!hovered && pressed) scale = 1

    // zoom aniamtion for press and release the button
    Behavior on scale {
        NumberAnimation {
            duration: 100
            easing.type: Easing.InOutQuad
        }
    }

    states: [
        State {
            name: GeneratorButton.XState.Shutdown
            PropertyChanges {
                target: control
                enabled: true
                text: qsTr("BUTTON: Start") + infoBarCtrl.emptyString
            }
            PropertyChanges { target: bg; color: gui.gButtonActiveBgColor }
        },
        State {
            name: GeneratorButton.XState.WaitingForStarting
            PropertyChanges {
                target: control
                enabled: false
                text: qsTr("BUTTON: Starting") + infoBarCtrl.emptyString
            }
            PropertyChanges { target: bg; color: gui.gButtonDisabledBgColor }
        },
        State {
            name: GeneratorButton.XState.WaitingForShutdown
            PropertyChanges {
                target: control
                enabled: false
                text: qsTr("BUTTON: Shutting down") + infoBarCtrl.emptyString
            }
            PropertyChanges { target: bg; color: gui.gButtonDisabledBgColor }
        },
        State {
            name: GeneratorButton.XState.Running
            PropertyChanges {
                target: control
                enabled: true
                text: qsTr("BUTTON: Shutdown") + infoBarCtrl.emptyString
            }
            PropertyChanges { target: bg; color: gui.gButtonBgColor }
        },
        State {
            name: GeneratorButton.XState.OkStarting
            PropertyChanges {
                target: control
                enabled: true
                text: qsTr("BUTTON: OK") + infoBarCtrl.emptyString
            }
            PropertyChanges { target: bg; color: gui.gButtonOkBgColor }
        },
        State {
            name: GeneratorButton.XState.OkStopping
            PropertyChanges {
                target: control
                enabled: true
                text: qsTr("BUTTON: OK") + infoBarCtrl.emptyString
            }
            PropertyChanges { target: bg; color: gui.gButtonBgColor }
        },
        State {
            name: GeneratorButton.XState.Cancel
            PropertyChanges {
                target: control
                enabled: true
                text: qsTr("BUTTON: Cancel") + infoBarCtrl.emptyString
            }
            PropertyChanges { target: bg; color: gui.gButtonCancelBgColor }
        }
    ]
}
