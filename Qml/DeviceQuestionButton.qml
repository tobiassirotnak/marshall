import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: control
    width: gui.dButtonWidth
    height: parent.height - 10/gui.scale
    anchors.verticalCenter: parent.verticalCenter
    // don't activate the button when finger was moving away from button (activate only when finger comes back)
    onHoveredChanged: if(!hovered && pressed) scale = 1

    contentItem: Column {
        width: parent.width
        height: parent.height
        DefaultText {
            text: control.text
            width: parent.width
            height: parent.height
            font.pixelSize: gui.dButtonTextSize
            font.bold: true
            color: bg.color == "#ffffff" ? "black" : gui.dButtonTextColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
    }

    background: Rectangle {
        id: bg
        implicitWidth: parent.width
        implicitHeight: parent.height
        radius: gui.mainItemRadius
    }

    onPressed: { scale = 0.95; gui.playPress() }
    onReleased: { scale = 1; gui.playRelease(); if(state !== "Hide") deviceListCtrl.tryToChangeState(index+1, state) }

    // zoom aniamtion for press and release the button
    Behavior on scale {
        NumberAnimation {
            duration: 100
            easing.type: Easing.InOutQuad
        }
    }

    states: [
        State {
            name: "OK_AskForTurningOff"
            PropertyChanges { target: control; text: qsTr("BUTTON: OK") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonBgColor }
        },
        State {
            name: "OK_AskForTurningOn"
            PropertyChanges { target: control; text: qsTr("BUTTON: OK") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonActiveBgColor }
        },
        State {
            name: "Cancel"
            PropertyChanges { target: control; text: qsTr("BUTTON: Cancel") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonCancelBgColor }
        },
        State {
            name: "Hide"
            PropertyChanges { target: control; text: "" + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: "transparent" }
        },
        State {
            name: "Turn_On"
            PropertyChanges { target: control; text: qsTr("BUTTON: Turn-on") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonBgColor }
        },
        State {
            name: "OK_AskForOverride"
            PropertyChanges { target: control; text: qsTr("BUTTON: OK") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonAttentionBgColor }
        },
        State {
            name: "OK_AskForReset"
            PropertyChanges { target: control; text: qsTr("BUTTON: OK") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonAttentionBgColor }
        },
        State {
            name: "Turn_Off"
            PropertyChanges { target: control; text: qsTr("BUTTON: Turn-off") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonActiveBgColor }
        },
        State {
            name: "Override"
            PropertyChanges { target: control; text: qsTr("BUTTON: Override") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonAttentionBgColor }
        },
        State {
            name: "Wait_ForRunning"
            PropertyChanges { target: control; text: qsTr("BUTTON: Wait_ForRunning") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonWaitingBgColor }
        },
        State {
            name: "Wait_ForOff"
            PropertyChanges { target: control; text: qsTr("BUTTON: Wait_ForOff") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonWaitingBgColor }
        },
        State {
            name: "Wait_ForOverride"
            PropertyChanges { target: control; text: qsTr("BUTTON: Wait_ForOverride") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonWaitingBgColor }
        },
        State {
            name: "Wait_ForReset"
            PropertyChanges { target: control; text: qsTr("BUTTON: Wait_ForReset") + infoBarCtrl.emptyString }
            PropertyChanges { target: bg; color: gui.dButtonWaitingBgColor }
        }
    ]
}
