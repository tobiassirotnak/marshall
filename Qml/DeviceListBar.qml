import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item {
    id: control
    x: gui.mainWidth
    y: gui.dlY
    width: gui.dlWidth
    height: gui.dlHeight

    Rectangle {
        anchors.fill: parent; color: gui.dlBgColor; radius: gui.mainItemRadius
        Rectangle { width: parent.radius; height: parent.height; color: parent.color; anchors.right: parent.right }
        Rectangle { width: parent.width; height: parent.radius; color: parent.color; anchors.bottom: parent.bottom }
    }

    states: State {
            name: "change_position";
            PropertyChanges { target: control; x: gui.dlX }
        }

    SequentialAnimation {
        id: startup_animation
        PauseAnimation { duration: gui.adDuration }
        PropertyAnimation { target: control; property: "x"; to: gui.dlX; duration: gui.startAppAnimationDuration }
        onFinished: control.state = "change_position"
    }

    // Animation at the beginning
    Component.onCompleted: {
        startup_animation.start()
    }

    Column {
        width: parent.width - 2*padding
        padding: 30/gui.scale
        Repeater {
            id: deviceListModel
            model: deviceListCtrl ? deviceListCtrl.model : 0
            DeviceBar { state: edit ? edit.state : "" }
        }
    }
}
