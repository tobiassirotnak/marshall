import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item {
    id: control
    width: parent.width
    height: 1024/deviceListModel.count/gui.scale
    //Rectangle { anchors.fill: parent; color: "darkgray"; border.width: 1; border.color: "gray" }

    // Since the device data are saved in Qt::EditRole, it is used edit as the role name
    property int actualPower: edit ? edit.actualPower : 0 // in W
    property int actualPowerBarLength: edit ? edit.actualPowerBarLength : 0 // in % (max 100%)
    property int remainingMinutes: edit ? edit.remainingMinutes : 0 // in min
    property int midPower: edit ? edit.midPower : 0 // in %

    onStateChanged: {
        if(control.state == "Running_Power_Low")
            gui.playActionRequired()
        console.log(index + ". DeviceBar state: " + state)
    }

    Row {
        height: parent.height
        DefaultText {
            width: gui.dTextWidth
            height: parent.height
            text: edit ? edit.name : "" // "Device " + (index+1)
            color: gui.dTitleColor
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: gui.dTitleTextSize
            font.bold: true
            rightPadding: 10
        }
        Rectangle { width: 1; height: parent.height - 5/gui.scale; color: gui.splitLineColor; anchors.verticalCenter: parent.verticalCenter }

        Row {
            id: mainRow
            height: parent.height
            Rectangle {
                id: powerRect
                width: (control.state=="Running" || control.state=="Running_Normal") ? gui.dPowerBarWidth*actualPowerBarLength/100 : control.state=="Off" ? gui.dPowerBarOffWidth : 0
                height: parent.height/2 /*dPowerBarHeight*/
                color: gui.dPowerBarColor
                anchors.verticalCenter: parent.verticalCenter

                // Average power bar
                Rectangle { width: gui.dPowerBarWidth*midPower/100; height: gui.dAveragePowerBarHeight; color: gui.dAveragePowerBarColor; anchors.top: parent.bottom; visible: (control.state=="Running" || control.state=="Running_Normal") }
                Rectangle { width: gui.dPowerBarOffWidth; height: gui.dAveragePowerBarHeight; color: gui.dAveragePowerBarColor; anchors.top: parent.bottom; visible: control.state=="Off" }

                DefaultText {
                    anchors.centerIn: parent
                    text: qsTr("POWER: %1W").arg(actualPower) + infoBarCtrl.emptyString
                    color: gui.dTitleColor
                    font.pixelSize: gui.dPowerbarTextSize
                    font.bold: false
                    visible: (control.state=="Running" || control.state=="Running_Normal") && width<parent.width
                }
                DefaultText {
                    anchors.left: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    leftPadding: 10
                    text: control.state=="Action_Required" ? qsTr("DESC: Action Required") + infoBarCtrl.emptyString : control.state=="Off" ? qsTr("DESC: Turn-on") : qsTr("POWER: %1W").arg(actualPower) + infoBarCtrl.emptyString
                    color: gui.dTitleColor
                    font.pixelSize: gui.dPowerbarTextSize + (control.state=="Off" ? 0/gui.scale : 0) // 'OFF' should be bigger
                    font.bold: true
                    visible: ((control.state=="Running" || control.state=="Running_Normal") && width>=parent.width && actualPower>0) || control.state=="Off" || control.state=="Action_Required"
                }
            }
            DefaultText {
                id: info
                anchors.verticalCenter: parent.verticalCenter
                leftPadding: 10
                color: control.state=="Running_Power_Low" ? gui.dTitleColor : gui.disabledTextColor
                font.pixelSize: gui.dInfoTextSize
                font.bold: true
                font.family: gui.mainFont
                visible: text.length > 0
                wrapMode: Text.WordWrap
                width: gui.dPowerBarWidth
            }
            Rectangle { width: gui.dPowerBarWidth - powerRect.width + gui.dSpaceBeforeButton - (info.visible ? info.width : 0); height: 1; color: "transparent" }
            DeviceButton {
                id: button
                timeDesc: ((button.state=="Turn_Off" || button.state=="Turn_On") && remainingMinutes>0) ? qsTr("TIME: %1h%2min").arg(Math.floor(remainingMinutes/60)).arg((remainingMinutes%60)<10 ? "0"+remainingMinutes%60 : remainingMinutes%60) + infoBarCtrl.emptyString : ""
                onReleased: deviceListCtrl.tryToChangeState(index+1, state)
            }
        }

        Row {
            id: buttonQuestionRow
            height: parent.height
            visible: false
            spacing: 20/gui.scale
            leftPadding: 50/gui.scale

            DeviceQuestionButton { id: button1 }
            DeviceQuestionButton { id: button2 }
            DeviceQuestionButton { id: button3 }
        }
    }

    states: [
        // Basic Turn-on(Off)/Turn-off(Running) states
        State {
            name: "Running_Normal"
            PropertyChanges { target: mainRow; visible: true }
            PropertyChanges { target: buttonQuestionRow; visible: false }
            PropertyChanges { target: button; state: "Turn_Off" }
            PropertyChanges { target: info; text: "" }
        },
        State {
            name: "Running"
            PropertyChanges { target: mainRow; visible: true }
            PropertyChanges { target: buttonQuestionRow; visible: false }
            PropertyChanges { target: button; state: "Turn_Off" }
            PropertyChanges { target: info; text: "" }
        },
        State {
            name: "Off"
            PropertyChanges { target: mainRow; visible: true }
            PropertyChanges { target: buttonQuestionRow; visible: false }
            PropertyChanges { target: button; state: "Turn_On" }
            PropertyChanges { target: info; text: "" }
        },
        State {
            name: "AskForTurningOn"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "OK_AskForTurningOn" }
            PropertyChanges { target: button2; state: "Cancel" }
            PropertyChanges { target: button3; state: "Hide" }
        },
        State {
            name: "AskForTurningOff"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "OK_AskForTurningOff" }
            PropertyChanges { target: button2; state: "Cancel" }
            PropertyChanges { target: button3; state: "Hide" }
        },
        State {
            name: "WaitForRunning"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "Wait_ForRunning" }
            PropertyChanges { target: button2; state: "Hide" }
            PropertyChanges { target: button3; state: "Hide" }
        },
        State {
            name: "WaitForOff"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "Wait_ForOff" }
            PropertyChanges { target: button2; state: "Hide" }
            PropertyChanges { target: button3; state: "Hide" }
        },
        // States for Add. Source
        State {
            name: "Disabled_Missing_Power"
            PropertyChanges { target: mainRow; visible: true }
            PropertyChanges { target: buttonQuestionRow; visible: false }
            PropertyChanges { target: button; state: "Add_Source" }
            PropertyChanges { target: info; text: qsTr("DESC: Add. Source") + infoBarCtrl.emptyString }
        },
        State {
            name: "AskForSource"
            PropertyChanges { target: mainRow; visible: true }
            PropertyChanges { target: buttonQuestionRow; visible: false }
            PropertyChanges { target: button; state: "Add_Source" }
            PropertyChanges { target: info; text: qsTr("DESC: Add. Source") + infoBarCtrl.emptyString }
        },
        // States for Override
        State {
            name: "Disabled_Power_Low"
            PropertyChanges { target: mainRow; visible: true }
            PropertyChanges { target: buttonQuestionRow; visible: false }
            PropertyChanges { target: button; state: "Override" }
            PropertyChanges { target: info; text: qsTr("DESC: Override") + infoBarCtrl.emptyString }
        },
        State {
            name: "AskForOverride"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "OK_AskForOverride" }
            PropertyChanges { target: button2; state: "Cancel" }
            PropertyChanges { target: button3; state: "Hide" }
        },
        State {
            name: "WaitForOverride"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "Wait_ForOverride" }
            PropertyChanges { target: button2; state: "Hide" }
            PropertyChanges { target: button3; state: "Hide" }
        },
        // States for Reset
        State {
            name: "Disabled_Overload"
            PropertyChanges { target: mainRow; visible: true }
            PropertyChanges { target: buttonQuestionRow; visible: false }
            PropertyChanges { target: button; state: "Reset" }
            PropertyChanges { target: info; text: qsTr("DESC: Reset") + infoBarCtrl.emptyString }
        },
        State {
            name: "AskForReset"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "OK_AskForReset" }
            PropertyChanges { target: button2; state: "Cancel" }
            PropertyChanges { target: button3; state: "Hide" }
        },
        State {
            name: "WaitForReset"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "Wait_ForReset" }
            PropertyChanges { target: button2; state: "Hide" }
            PropertyChanges { target: button3; state: "Hide" }
        },
        // States for Action Required
        State {
            name: "Running_Power_Low"
            PropertyChanges { target: mainRow; visible: true }
            PropertyChanges { target: buttonQuestionRow; visible: false }
            PropertyChanges { target: button; state: "Action_Required" }
            PropertyChanges { target: info; text: qsTr("DESC: Turn off recommended") + infoBarCtrl.emptyString }
        },
        State {
            name: "AskForAction"
            PropertyChanges { target: mainRow; visible: false }
            PropertyChanges { target: buttonQuestionRow; visible: true }
            PropertyChanges { target: button1; state: "Turn_Off" }
            PropertyChanges { target: button2; state: "Override" }
            PropertyChanges { target: button3; state: "Cancel" }
        }
    ]
}
