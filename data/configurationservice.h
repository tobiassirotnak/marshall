#ifndef CONFIGURATIONSERVICE_H
#define CONFIGURATIONSERVICE_H

#include <QObject>

namespace data {

    enum class DeviceState
    {
        Off,
        Override,
        ActionRequired,
        AddSource,
        On
    };

    struct DeviceStateDescriptor
    {
        DeviceState MainGen;
        DeviceState APU;
        DeviceState LT100;
        DeviceState LT80;
        DeviceState LT60;
        DeviceState LT40;
        DeviceState LT20;
    };

    struct DeviceDescriptor
    {
        QString Name;
        int MaxValue;
        data::DeviceStateDescriptor EnergyLimits;
    };

    class ConfigurationService : public QObject
    {
        Q_OBJECT
    public:
        explicit ConfigurationService(QObject *parent = nullptr);

        QVector<data::DeviceDescriptor> getDeviceList();
        QVector<data::DeviceStateDescriptor> getDefaultCustomProfile();
        QVector<data::DeviceStateDescriptor> getStandardProfile();
        QVector<data::DeviceStateDescriptor> getSilentWatchProfile();
        QVector<data::DeviceStateDescriptor> getPro3Profile();

    private:

        enum class ProfileType
        {
            Custom,
            Standard,
            SilentWatch,
            Profile3
        };

        QJsonArray getDevicesJasonObjectArray();
        QVector<data::DeviceStateDescriptor> getProfile(data::ConfigurationService::ProfileType aType);
        data::DeviceState getDeviceState(const QString& aValue);

    signals:

    };
}

#endif // CONFIGURATIONSERVICE_H
