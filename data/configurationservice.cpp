#include <QFile>
#include <QDir>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QString>

#include "configurationservice.h"

const QString cDeviceFileName = "./Devices.json";
const QString cDevicesTag = "Devices";
const QString cDeviceNameTag = "Name";
const QString cDeviceMaxValueTag = "MaxValue";
const QString cDeviceProfileListTag = "Profiles";
const QString cDeviceProfileCustomTag = "Custom";
const QString cDeviceProfileStandardTag = "Standard";
const QString cDeviceProfileSilentWatchTag = "SilentWatch";
const QString cDeviceProfileProfile3Tag = "Profile3";
const QString cDeviceProfileNameTag = "ProfileName";
const QString cDeviceProfileStatesTag = "States";

const QString cValueMG = "MG";
const QString cValueAPU = "APU";
const QString cValueLT100 = "<100";
const QString cValueLT80 = "<80";
const QString cValueLT60 = "<60";
const QString cValueLT40 = "<40";
const QString cValueLT20 = "<20";

const QString cValueOFF = "OFF";
const QString cValueON = "ON";
const QString cValueOverride = "OVERRIDE";
const QString cValueAddSrc = "ADDSRC";
const QString cValueActionReq = "ACTIONREQ";

namespace data {

    ConfigurationService::ConfigurationService(QObject *parent) : QObject(parent)
    {
    }

    QVector<data::DeviceDescriptor> ConfigurationService::getDeviceList()
    {
        QVector<data::DeviceDescriptor> lDeviceDescrList;

        QJsonArray lObjectArray = getDevicesJasonObjectArray();
        for (int i = 0; i < lObjectArray.size(); ++i)
        {
            data::DeviceDescriptor lDescr;
            QJsonObject lObj= lObjectArray[i].toObject();
            lDescr.Name = lObj.value(cDeviceNameTag).toString();
            lDescr.MaxValue = lObj.value(cDeviceMaxValueTag).toInt();
            lDescr.EnergyLimits.MainGen = DeviceState::Off;
            lDescr.EnergyLimits.APU = DeviceState::Off;
            lDescr.EnergyLimits.LT100 = DeviceState::Off;
            lDescr.EnergyLimits.LT80 = DeviceState::Off;
            lDescr.EnergyLimits.LT60 = DeviceState::Off;
            lDescr.EnergyLimits.LT40 = DeviceState::Off;
            lDescr.EnergyLimits.LT20 = DeviceState::Off;
            lDeviceDescrList.append(lDescr);
        }

        return lDeviceDescrList;
    }

    QVector<DeviceStateDescriptor> ConfigurationService::getDefaultCustomProfile()
    {
        return getProfile(data::ConfigurationService::ProfileType::Standard);
    }

    QVector<DeviceStateDescriptor> ConfigurationService::getStandardProfile()
    {
        return getProfile(data::ConfigurationService::ProfileType::Standard);
    }

    QVector<DeviceStateDescriptor> ConfigurationService::getSilentWatchProfile()
    {
        return getProfile(data::ConfigurationService::ProfileType::SilentWatch);
    }

    QVector<DeviceStateDescriptor> ConfigurationService::getPro3Profile()
    {
        return getProfile(data::ConfigurationService::ProfileType::Profile3);
    }

    QJsonArray ConfigurationService::getDevicesJasonObjectArray()
    {
        QJsonArray lObjectArray;

        QFileInfo lFileInfo(cDeviceFileName);
        if(lFileInfo.exists())
        {
            QDir lOldPath = QDir::currentPath();
            QDir::setCurrent(lFileInfo.path());
            QFile lJsonFile(lFileInfo.fileName());

            lJsonFile.open(QIODevice::ReadOnly);
            QByteArray lRawData = lJsonFile.readAll();
            lJsonFile.close();

            QDir::setCurrent(lOldPath.path());

            QJsonParseError lJsonError;
            QJsonDocument lDoc(QJsonDocument::fromJson(lRawData, &lJsonError));
            if(QJsonParseError::NoError != lJsonError.error)
            {
                qWarning() << "ERROR: Parsing json data: "<<lJsonError.errorString()<< " for data " << lRawData << " from file " << cDeviceFileName;
            }
            QJsonObject lRootObj = lDoc.object();
            lObjectArray = lRootObj[cDevicesTag].toArray();
        }
        else
        {
            qDebug() << "Could not read " << cDeviceFileName;
        }

        return lObjectArray;
    }

    QVector<DeviceStateDescriptor> ConfigurationService::getProfile(data::ConfigurationService::ProfileType aType)
    {
        QVector<DeviceStateDescriptor> lStateDescrList;

        QString lProfileTag = cDeviceProfileCustomTag;
        switch(aType)
        {
            case ProfileType::Custom:
            {
                lProfileTag = cDeviceProfileCustomTag;
                break;
            }
            case ProfileType::Standard:
            {
                lProfileTag = cDeviceProfileStandardTag;
                break;
            }
            case ProfileType::SilentWatch:
            {
                lProfileTag = cDeviceProfileSilentWatchTag;
                break;
            }
            case ProfileType::Profile3:
            {
                lProfileTag = cDeviceProfileProfile3Tag;
                break;
            }
        }

        QJsonArray lDevicesArray = getDevicesJasonObjectArray();
        for (int i = 0; i < lDevicesArray.size(); ++i)
        {
            QJsonObject lDeviceObj= lDevicesArray[i].toObject();
            QJsonArray lProfileArray = lDeviceObj[cDeviceProfileListTag].toArray();
            for (int idx = 0; idx < lProfileArray.size(); ++idx)
            {
                data::DeviceStateDescriptor lDeviceStateDescr;
                QJsonObject lProfileObj= lProfileArray[idx].toObject();
                if(lProfileObj.value(cDeviceProfileNameTag) == lProfileTag)
                {                    
                    QJsonObject lObj= lProfileObj.value(cDeviceProfileStatesTag).toObject();
                    lDeviceStateDescr.MainGen = getDeviceState(lObj.value(cValueMG).toString());
                    lDeviceStateDescr.APU = getDeviceState(lObj.value(cValueAPU).toString());
                    lDeviceStateDescr.LT100 = getDeviceState(lObj.value(cValueLT100).toString());
                    lDeviceStateDescr.LT80 = getDeviceState(lObj.value(cValueLT80).toString());
                    lDeviceStateDescr.LT60 = getDeviceState(lObj.value(cValueLT60).toString());
                    lDeviceStateDescr.LT40 = getDeviceState(lObj.value(cValueLT40).toString());
                    lDeviceStateDescr.LT20 = getDeviceState(lObj.value(cValueLT20).toString());

                    lStateDescrList.append(lDeviceStateDescr);
                }
            }
        }

        return lStateDescrList;
    }

    DeviceState ConfigurationService::getDeviceState(const QString& aValue)
    {
        DeviceState lDescr = DeviceState::Off;

        if(aValue.toUpper() == cValueOFF)
        {
            lDescr = DeviceState::Off;
        }
        else if(aValue.toUpper() == cValueON)
        {
            lDescr = DeviceState::On;
        }
        else if(aValue.toUpper() == cValueOverride)
        {
            lDescr = DeviceState::Override;
        }
        else if(aValue.toUpper() == cValueAddSrc)
        {
            lDescr = DeviceState::AddSource;
        }
        else if(aValue.toUpper() == cValueActionReq)
        {
            lDescr = DeviceState::ActionRequired;
        }

        return lDescr;
    }
}
