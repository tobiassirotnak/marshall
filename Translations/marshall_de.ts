<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>BatteryBar</name>
    <message>
        <location filename="../Qml/BatteryBar.qml" line="57"/>
        <source>TITLE: Battery</source>
        <translation>DE: BATTERY</translation>
    </message>
    <message>
        <location filename="../Qml/BatteryBar.qml" line="72"/>
        <source>DESC: Estimeted time left: </source>
        <translation>DE: Estimated time left: </translation>
    </message>
    <message>
        <location filename="../Qml/BatteryBar.qml" line="15"/>
        <source>TIME: %1h%2min</source>
        <translation>DE: %1h%2min</translation>
    </message>
    <message>
        <location filename="../Qml/BatteryBar.qml" line="37"/>
        <source>DESC: Predicted Charge Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/BatteryBar.qml" line="40"/>
        <source>DESC: SoC</source>
        <translation>DE: SoC</translation>
    </message>
</context>
<context>
    <name>BatteryTimeBar</name>
    <message>
        <location filename="../Qml/BatteryTimeBar.qml" line="38"/>
        <source>TIME: H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/BatteryTimeBar.qml" line="51"/>
        <source>DESC: Silent watch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceBar</name>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="51"/>
        <location filename="../Qml/DeviceBar.qml" line="62"/>
        <source>POWER: %1W</source>
        <translation>DE: %1W</translation>
    </message>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="62"/>
        <source>DESC: Turn-on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="164"/>
        <location filename="../Qml/DeviceBar.qml" line="171"/>
        <source>DESC: Add. Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="179"/>
        <source>DESC: Override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="203"/>
        <source>DESC: Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="227"/>
        <source>DESC: Turn off recommended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="196"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="62"/>
        <source>DESC: Action Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DESC: ...</source>
        <translation type="vanished">DE: TBD ...</translation>
    </message>
    <message>
        <source>BUTTON: Turn-on</source>
        <translation type="vanished">DE: Turn-on</translation>
    </message>
    <message>
        <source>BUTTON: Turn-off</source>
        <translation type="vanished">DE: Turn-off</translation>
    </message>
    <message>
        <location filename="../Qml/DeviceBar.qml" line="85"/>
        <source>TIME: %1h%2min</source>
        <translation>DE: %1h%2min</translation>
    </message>
</context>
<context>
    <name>DeviceButton</name>
    <message>
        <location filename="../Qml/DeviceButton.qml" line="65"/>
        <source>BUTTON: Turn-off</source>
        <translation type="unfinished">DE: Turn-off</translation>
    </message>
    <message>
        <location filename="../Qml/DeviceButton.qml" line="70"/>
        <source>BUTTON: Turn-on</source>
        <translation type="unfinished">DE: Turn-on</translation>
    </message>
    <message>
        <location filename="../Qml/DeviceButton.qml" line="75"/>
        <source>BUTTON: Add. Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceButton.qml" line="80"/>
        <source>BUTTON: Override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceButton.qml" line="85"/>
        <source>BUTTON: Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceButton.qml" line="90"/>
        <source>BUTTON: Action Required</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceQuestionButton</name>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="50"/>
        <location filename="../Qml/DeviceQuestionButton.qml" line="55"/>
        <location filename="../Qml/DeviceQuestionButton.qml" line="75"/>
        <location filename="../Qml/DeviceQuestionButton.qml" line="80"/>
        <source>BUTTON: OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="60"/>
        <source>BUTTON: Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="70"/>
        <source>BUTTON: Turn-on</source>
        <translation type="unfinished">DE: Turn-on</translation>
    </message>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="85"/>
        <source>BUTTON: Turn-off</source>
        <translation type="unfinished">DE: Turn-off</translation>
    </message>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="90"/>
        <source>BUTTON: Override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="95"/>
        <source>BUTTON: Wait_ForRunning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="100"/>
        <source>BUTTON: Wait_ForOff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="105"/>
        <source>BUTTON: Wait_ForOverride</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/DeviceQuestionButton.qml" line="110"/>
        <source>BUTTON: Wait_ForReset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneratorBar</name>
    <message>
        <source>TITLE: Generator</source>
        <translation type="vanished">DE: GENERATOR</translation>
    </message>
    <message>
        <source>DESC: Main engine generator</source>
        <translation type="vanished">DE: Main-Engine Generator</translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorBar.qml" line="59"/>
        <source>STATE: Off</source>
        <translation>DE: OFF</translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorBar.qml" line="59"/>
        <source>STATE: Running</source>
        <translation>DE: Running</translation>
    </message>
    <message>
        <source>DESC: Auxiliary power unit</source>
        <translation type="vanished">DE: Auxiliary Power Unit</translation>
    </message>
</context>
<context>
    <name>GeneratorButton</name>
    <message>
        <location filename="../Qml/GeneratorButton.qml" line="62"/>
        <source>BUTTON: Start</source>
        <translation>DE: Start</translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorButton.qml" line="71"/>
        <source>BUTTON: Starting</source>
        <translation>DE: Starting</translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorButton.qml" line="80"/>
        <source>BUTTON: Shutting down</source>
        <translation>DE: Shutting down</translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorButton.qml" line="89"/>
        <source>BUTTON: Shutdown</source>
        <translation>DE: Shutdown</translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorButton.qml" line="98"/>
        <location filename="../Qml/GeneratorButton.qml" line="107"/>
        <source>BUTTON: OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorButton.qml" line="116"/>
        <source>BUTTON: Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneratorListBar</name>
    <message>
        <location filename="../Qml/GeneratorListBar.qml" line="23"/>
        <source>TITLE: Generator</source>
        <translation type="unfinished">DE: GENERATOR</translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorListBar.qml" line="36"/>
        <source>DESC: Main engine generator</source>
        <translation type="unfinished">DE: Main-Engine Generator</translation>
    </message>
    <message>
        <location filename="../Qml/GeneratorListBar.qml" line="46"/>
        <source>DESC: Auxiliary power unit</source>
        <translation type="unfinished">DE: Auxiliary Power Unit</translation>
    </message>
</context>
<context>
    <name>InfoBar</name>
    <message>
        <location filename="../Qml/InfoBar.qml" line="92"/>
        <source>TITLE: Dashboard</source>
        <translation>DE: DASHBOARD</translation>
    </message>
</context>
<context>
    <name>PowerSourcesBar</name>
    <message>
        <location filename="../Qml/PowerSourcesBar.qml" line="25"/>
        <source>TITLE: Power sources</source>
        <translation>DE: POWER SOURCES</translation>
    </message>
</context>
<context>
    <name>PowerSourcesCtrl</name>
    <message>
        <source>INFO: Timeout cannot start main generator</source>
        <translation type="vanished">DE: Timeout: Cannot start Main-Engine Generator</translation>
    </message>
    <message>
        <source>INFO: Cannot start main generator</source>
        <translation type="vanished">DE: Cannot start Main-Engine Generator</translation>
    </message>
    <message>
        <source>INFO: Timeout cannot start APU generator</source>
        <translation type="vanished">DE: Timeout: Cannot start APU</translation>
    </message>
    <message>
        <source>INFO: Cannot start APU generator</source>
        <translation type="vanished">DE: Cannot start APU</translation>
    </message>
</context>
<context>
    <name>ProfilesBar</name>
    <message>
        <location filename="../Qml/ProfilesBar.qml" line="54"/>
        <source>TITLE: Profiles</source>
        <translation>DE: PROFILES</translation>
    </message>
    <message>
        <location filename="../Qml/ProfilesBar.qml" line="70"/>
        <source>BUTTON: Custom</source>
        <translation>DE: Custom</translation>
    </message>
    <message>
        <location filename="../Qml/ProfilesBar.qml" line="87"/>
        <source>BUTTON: Default</source>
        <translation>DE: Standard</translation>
    </message>
    <message>
        <location filename="../Qml/ProfilesBar.qml" line="105"/>
        <source>BUTTON: Profile1</source>
        <translation>DE: Silent watch</translation>
    </message>
    <message>
        <location filename="../Qml/ProfilesBar.qml" line="106"/>
        <location filename="../Qml/ProfilesBar.qml" line="124"/>
        <source>BUTTON TIME: %1h%2min</source>
        <translation>DE: %1h%2min</translation>
    </message>
    <message>
        <location filename="../Qml/ProfilesBar.qml" line="123"/>
        <source>BUTTON: Profile2</source>
        <translation>DE: Profile #3</translation>
    </message>
</context>
<context>
    <name>ProfilesButton</name>
    <message>
        <location filename="../Qml/ProfilesButton.qml" line="23"/>
        <source>Button: OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimMainWindow</name>
    <message>
        <location filename="../Qml/SimMainWindow.qml" line="12"/>
        <source>Animation of values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/SimMainWindow.qml" line="22"/>
        <source>PowerSources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/SimMainWindow.qml" line="31"/>
        <source>Start MainGenerator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/SimMainWindow.qml" line="41"/>
        <source>Stop MainGenerator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/SimMainWindow.qml" line="51"/>
        <source>Start AuxPowerUnit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/SimMainWindow.qml" line="61"/>
        <source>Stop AuxPowerUnit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qml/SimMainWindow.qml" line="70"/>
        <source>Battery SoC in %</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimulatorApp</name>
    <message>
        <location filename="../Qml/SimulatorApp.qml" line="11"/>
        <source>Simulator</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../Qml/main.qml" line="12"/>
        <source>WINDOW TITLE: Dashboard</source>
        <translation>DE: DASHBOARD</translation>
    </message>
</context>
</TS>
