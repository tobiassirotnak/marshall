Application to control input power sources (batteries, generators) in vehicles. It also displays estimated battery life and supports power profiles. 


### Minimum requirements

* Qt 5.15.0

### Building

* Open QtCreator project file
* Build application
* Run executable file (with Devices.json file in same folder)

### Contributing
Expected workflow is: Fork -> Patch -> Push -> Pull Request


### Further information
For more information and instructions about how to install Marshall, please get in contact with us on email.


*Marshall is a nickname for special vehicle dashboard*
