#ifndef BATTERY_H
#define BATTERY_H

#include <QObject>

namespace domain {

    class Battery : public QObject
    {
        Q_OBJECT
    public:
        explicit Battery(QObject *parent = nullptr);

        int getActualRestTime() const;
        void setRestTime(int aTime);
        void setStateOfCharge(int aValue);
        int getStateOfCharge();

    private:
        int mRestEnduranceInMinutes;
        int mMaxEnduranceInMinutes;
        int mStateOfCharge;

        void calculateRestTime();

    signals:
        void estimatedTimeChanged(int,int);
        void silentWatchTimesChanged(QVector<int>);
        void predictedChargeTimeChanged(QVector<int>);
        void stateOfChargeChanged();
    };
}

#endif // BATTERY_H
