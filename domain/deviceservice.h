#ifndef DEVICESERVICE_H
#define DEVICESERVICE_H

#include <QObject>

#include "devicesimulator.h"
#include "data/configurationservice.h"


namespace domain {

    class Device;    

    struct DeviceDescriptor
    {
        QString Name;
        int CommandState;
        int MidValue;       
    };

    class DeviceService : public QObject
    {
        Q_OBJECT

        friend class domain::DeviceSimulator;

    public:

        enum DeviceProfile
        {
            Custom = 1,
            Standard,
            SilentWatch,
            Profile3
        };

        enum class DeviceProfileState
        {
            Disabled_Missing_Power,
            Disabled_Power_Low,
            Disabled_Overload,
            Running_Power_Low,
            Running_Normal,
            Off,
            Off_DoNotTurnOn
        };

        enum class CommandSubState
        {
            AskForTurningOn,
            WaitForRunning,
            Running,
            RunningLow,
            AskForTurningOff,
            WaitForOff,
            Off,
            AskForAction,
            AskForOverride,
            WaitForOverride,
            Override,
            WaitForDisable,
            Disabled,
            AskForReset,
            WaitForReset,
            AskForSource
        };

        enum class UserCommand
        {
            Undefined,
            Ok,
            Cancel,
            Action_Required,
            Add_Source,
            Add_Main_Engine,
            Add_APU,
            Turn_On,
            Turn_Off,
            Reset,
            Override
        };



        explicit DeviceService(QObject *parent = nullptr);

        void loadDevices();
        void loadProfiles();
        void startupSystem();

        QVector<domain::DeviceDescriptor> getDeviceDescr(int aProfileNr);
        void setDeviceCommand(int aDeviceNr, UserCommand aCmd);

        void setProfile(int aProfileNr);
        domain::DeviceService::DeviceProfile getSelectedProfile();

        void setConfigurationService(data::ConfigurationService* aConfigurationService);

    public slots:
        void setActualValue(int aDeviceNr, int aCalcValue, int aRealValue);
        void setBatteryStateOfCharge(int aValue, bool isMainGenOn, bool isApuOn);
        void setMidValue(int aDeviceNr, int aValue);
        void setRestTime(int aDeviceNr, int aRestTime);
        void setToCustomProfile();        

    private:

        QVector<Device*> mDeviceList;
        QVector<QVector<data::DeviceStateDescriptor>> mProfileList;
        DeviceProfile mSelectedProfile;
        data::ConfigurationService* mConfigurationService;

        QVector<Device*> getDeviceList();
        QVector<data::DeviceStateDescriptor> cloneDeviceListProfileData();

    signals:
        void deviceStateChanged(int aDeviceNr,domain::DeviceDescriptor);
        void actualValueChanged(int aDeviceNr,int aPercent,int aRealVal);
        void midValueChanged(int aDeviceNr,int aPercent);
        void restTimeChanged(int aDeviceNr,int aAmountMinutes);
        void switchToProfile(int aProfileNr);
        void profileChanged();
    };
}

#endif // DEVICESERVICE_H
