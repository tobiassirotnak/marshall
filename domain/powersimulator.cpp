#include "powersimulator.h"
#include "powersourceservice.h"
#include "maingenerator.h"
#include "auxpowerunit.h"
#include "battery.h"

namespace domain {

    PowerSimulator::PowerSimulator(QObject *parent) : QObject(parent),
        mPowerSourceService(nullptr),
        mPowerUpdateTimer()
    {
        connect(&mPowerUpdateTimer,&QTimer::timeout,this,&PowerSimulator::updateBatteryStateOfCharge);
    }

    void PowerSimulator::setPowerSourceService(PowerSourceService *aPowerSourceService)
    {
        mPowerSourceService = aPowerSourceService;
    }

    void PowerSimulator::startMainGenerator()
    {
        if(mPowerSourceService != nullptr)
        {
            mPowerSourceService->getMainGenerator()->setState(PowerSourceService::PowerSourceState::Running);
            mPowerUpdateTimer.start(500);
        }
    }

    void PowerSimulator::stopMainGenerator()
    {
        if(mPowerSourceService != nullptr)
        {
            mPowerSourceService->getMainGenerator()->setState(PowerSourceService::PowerSourceState::Stopped);
            mPowerUpdateTimer.stop();
        }
    }

    void PowerSimulator::startAuxPowerUnit()
    {
        if(mPowerSourceService != nullptr)
        {
            mPowerSourceService->getAuxPowerUnit()->setState(PowerSourceService::PowerSourceState::Running);
            mPowerUpdateTimer.start(100);
        }
    }

    void PowerSimulator::stopAuxPowerUnit()
    {
        if(mPowerSourceService != nullptr)
        {
            mPowerSourceService->getAuxPowerUnit()->setState(PowerSourceService::PowerSourceState::Stopped);
            mPowerUpdateTimer.stop();
        }
    }

    void PowerSimulator::setBatteryStateOfCharge(int aValue)
    {
        mPowerSourceService->getBattery()->setStateOfCharge(aValue);
    }

    void PowerSimulator::updateBatteryStateOfCharge()
    {
        int lNewSoC = mPowerSourceService->getBattery()->getStateOfCharge();
        if((mPowerSourceService->getMainGenerator()->isRunning()) ||
            (mPowerSourceService->getAuxPowerUnit()->isRunning()))
        {
            // load Battery
            lNewSoC += 1;
            if(lNewSoC > 100)
            {
                lNewSoC = 100;
            }
            mPowerSourceService->getBattery()->setStateOfCharge(lNewSoC);
        }
    }
}
