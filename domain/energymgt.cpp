#include "energymgt.h"
#include "battery.h"
#include "maingenerator.h"
#include "auxpowerunit.h"

namespace domain {

    EnergyMgt::EnergyMgt(QObject *parent) : QObject(parent),
        mMainGenerator(nullptr),
        mAuxPowerUnit(nullptr),
        mBattery(nullptr)
    {

    }

    void EnergyMgt::setMainGenerator(MainGenerator *aMainGenerator)
    {
        mMainGenerator = aMainGenerator;
    }

    void EnergyMgt::setAuxPowerUnit(AuxPowerUnit *aAuxPowerUnit)
    {
        mAuxPowerUnit = aAuxPowerUnit;
    }

    void EnergyMgt::setBattery(Battery *aBattery)
    {
        mBattery = aBattery;
    }

    int EnergyMgt::getBatteryStateOfCharge()
    {
        return mBattery->getStateOfCharge();
    }

    void EnergyMgt::setBatteryStateOfCharge()
    {
        emit batteryStateOfChargeChanged(mBattery->getStateOfCharge(), mMainGenerator->isRunning(), mAuxPowerUnit->isRunning());
    }

    void EnergyMgt::startMainGenerator()
    {
        setBatteryStateOfCharge();
    }

    void EnergyMgt::stopMainGenerator()
    {
        setBatteryStateOfCharge();
    }

    void EnergyMgt::startApuGenerator()
    {
        setBatteryStateOfCharge();
    }

    void EnergyMgt::stopApuGenerator()
    {
        setBatteryStateOfCharge();
    }
}
