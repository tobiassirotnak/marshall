#ifndef DEVICEFSM_H
#define DEVICEFSM_H

#include <QObject>
#include <QStateMachine>

#include "deviceservice.h"

namespace domain {

    class Device;

    class DeviceFSM : public QObject
    {
        Q_OBJECT
    public:
        explicit DeviceFSM(QObject *parent = nullptr);

        void initStates();

        bool isRunning();
        bool isOnNormal();
        bool isOnLow();

        bool isDisabledLow();
        bool isDisabledOverload();
        bool isDisabledNoPower();

        bool isOff();

    private:
        Device* mDevice;
        QStateMachine mFSM;
        QState* mInitialized;

        QState* mOperatingState;
        QState* mOn;
        QState* mOff;
        QState* mDisabled;
        QState* mFromOffToOn;
        QState* mFromOffToDisabled;
        QState* mFromOnToOff;
        QState* mFromOnToDisabled;
        QState* mFromDisabledToOff;
        QState* mFromDisabledToOn;

        QState* mStateOfChargeLevel;
        QState* mSocNormal;
        QState* mSocLow;
        QState* mSocOff;
        QState* mSocOverload;

        DeviceService::DeviceProfileState getStateOfChargeLevel();
        DeviceService::CommandSubState getCommandSubState();

    private slots:
        void onEnteredOn();
        void onEnteredFromOnToOff();
        void onEnteredOff();
        void onEnteredFromOnToDisabled();
        void onEnteredDisabled();
        void onEnteredFromOffToOn();
        void onEnteredFromOffToDisabled();
        void onEnteredFromDisabledToOff();
        void onEnteredFromDisabledToOn();

        void onEnteredSocNormal();
        void onEnteredSocLow();
        void onEnteredSocOff();
        void onEnteredSocOverload();

    signals:
        void switchOperationOn();
        void switchOperationOnDone();
        void switchOperationOff();
        void switchOperationOffDone();
        void switchOperationDisabled();
        void switchOperationDisabledDone();
        void switchOperationCancel();

        void switchSocNormal();
        void switchSocLow();
        void switchSocOff();
        void switchSocOverload();

        void stateChanged(DeviceService::CommandSubState);

    };
}

#endif // DEVICEFSM_H
