#ifndef POWERSIMULATOR_H
#define POWERSIMULATOR_H

#include <QObject>
#include <QTimer>

namespace domain {

    class PowerSourceService;

    class PowerSimulator : public QObject
    {
        Q_OBJECT
    public:
        explicit PowerSimulator(QObject *parent = nullptr);

        void setPowerSourceService(PowerSourceService *aPowerSourceService);

        void startMainGenerator();
        void stopMainGenerator();
        void startAuxPowerUnit();
        void stopAuxPowerUnit();
        void setBatteryStateOfCharge(int aValue);

    private:
        domain::PowerSourceService* mPowerSourceService;                
        QTimer mPowerUpdateTimer;

        void updateBatteryStateOfCharge();

    signals:

    };
}

#endif // POWERSIMULATOR_H
