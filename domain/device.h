#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>
#include <QTimer>
#include "devicefsm.h"
#include "deviceservice.h"
#include "data/configurationservice.h"
#include "devicesimulator.h"

namespace domain {

    class Device : public QObject
    {
        Q_OBJECT

        friend class domain::DeviceSimulator;

    public:

        explicit Device(int aId, QObject *parent = nullptr);

        DeviceDescriptor getDescriptor();

        void initFSM();

        bool isRunning();

        bool setUserCommand(DeviceService::UserCommand aCmd);

        void setProfileStates(data::DeviceStateDescriptor aProfileStateDescr);
        data::DeviceStateDescriptor getProfileStates();

        void setName(QString aName);
        void setActualValue(int aValue);
        void setRestTime(int aValue);
        void setMidValue(int aValue);
        void setMaxValue(int aValue);

        void setBatteryStateOfCharge(int aValue, bool isMainGenOn, bool isApuOn);
        void setEnergyLimits(data::DeviceStateDescriptor aLimit);

    public slots:
        void setState(DeviceService::CommandSubState aCmdSubState);

    signals:
        void actualValueChanged(int,int,int);
        void midValueChanged(int,int);
        void stateChanged(int,domain::DeviceDescriptor);
        void restTimeChanged(int,int);
        void profileStateChanged();

    private:
        int mId;
        QString mName;
        DeviceFSM mDeviceFSM;
        DeviceService::CommandSubState mCommandSubState;
        int mActualValue;
        int mMaxValue;
        int mMidValue;
        int mAvailableBatteryCharge;
        data::DeviceStateDescriptor mEnergyLimits;

        void checkSingleSocLimite(data::DeviceState aLimitState);
        int getCalculatedValue(int aValue);
        int getMidValue();
        int getMaxValue();
        int getActualValue();
        void checkEnergyLimits(bool isMainGenOn, bool isApuOn);
        bool setRunningNormalCommand(DeviceService::UserCommand aCmd);
        bool setRunningLowPowerCommand(DeviceService::UserCommand aCmd);
        bool setDisabledOverloadCommand(DeviceService::UserCommand aCmd);
        bool setDisabledLowPowerCommand(DeviceService::UserCommand aCmd);
        bool setDisabledMissingPowerCommand(DeviceService::UserCommand aCmd);        
    };
}
#endif // DEVICE_H
