#ifndef ENERGYMGT_H
#define ENERGYMGT_H

#include <QObject>

namespace domain {

    class MainGenerator;
    class AuxPowerUnit;
    class Battery;

    class EnergyMgt : public QObject
    {
        Q_OBJECT
    public:
        explicit EnergyMgt(QObject *parent = nullptr);

        void setMainGenerator(MainGenerator* aMainGenerator);
        void setAuxPowerUnit(AuxPowerUnit* aAuxPowerUnit);
        void setBattery(Battery* aBattery);

        int getBatteryStateOfCharge();

    public slots:
        void setBatteryStateOfCharge();
        void startMainGenerator();
        void stopMainGenerator();
        void startApuGenerator();
        void stopApuGenerator();

    public:
        MainGenerator* mMainGenerator;
        AuxPowerUnit* mAuxPowerUnit;
        Battery* mBattery;

    signals:
        void batteryStateOfChargeChanged(int,bool,bool);

    };
}

#endif // ENERGYMGT_H
