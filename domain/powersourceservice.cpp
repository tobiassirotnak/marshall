#include <QTimer>

#include "powersourceservice.h"
#include "maingenerator.h"
#include "auxpowerunit.h"
#include "battery.h"

namespace domain {

    PowerSourceService::PowerSourceService(QObject *parent) : QObject(parent),
        mMainGenerator(nullptr),
        mAuxPowerUnit(nullptr),
        mBattery(nullptr)
    {

    }

    void PowerSourceService::setMainGenerator(MainGenerator *aMainGenerator)
    {
        mMainGenerator = aMainGenerator;
    }

    void PowerSourceService::setAuxPowerUnit(AuxPowerUnit *aAuxPowerUnit)
    {
        mAuxPowerUnit = aAuxPowerUnit;
    }

    void PowerSourceService::setBattery(Battery *aBattery)
    {
        mBattery = aBattery;
    }

    void PowerSourceService::connectComponents()
    {        
        connect(mMainGenerator,&MainGenerator::running,this,&PowerSourceService::setMainGeneratorRunning);
        connect(mMainGenerator,&MainGenerator::stopped,this,&PowerSourceService::setMainGeneratorStopped);
        connect(mMainGenerator,&MainGenerator::stateChanged,this,&PowerSourceService::mainGeneratorStateChanged);

        connect(mAuxPowerUnit,&AuxPowerUnit::running,this,&PowerSourceService::setApuRunning);
        connect(mAuxPowerUnit,&AuxPowerUnit::stopped,this,&PowerSourceService::setApuStopped);
        connect(mAuxPowerUnit,&AuxPowerUnit::stateChanged,this,&PowerSourceService::apuStateChanged);

        connect(mBattery,&Battery::estimatedTimeChanged,this,&PowerSourceService::setBatteryEstimatedTime);
        connect(mBattery,&Battery::silentWatchTimesChanged,this,&PowerSourceService::setSilentWatchTimes);
        connect(mBattery,&Battery::predictedChargeTimeChanged,this,&PowerSourceService::setPredictedTime);
    }

    void PowerSourceService::setPowerSourceCommand(bool isMainGen, PowerSourceService::UserCommand aCmd)
    {
        if(isMainGen)
        {
            if(mMainGenerator)
            {
                mMainGenerator->setUserCommand(aCmd);
            }
        }
        else
        {
            if(mAuxPowerUnit)
            {
                mAuxPowerUnit->setUserCommand(aCmd);
            }
        }
    }

    void PowerSourceService::startMainGenerator()
    {
        if(mMainGenerator != nullptr)
        {
            mMainGenerator->tryToStart();
        }
    }

    void PowerSourceService::stopMainGenerator()
    {
        if(mMainGenerator != nullptr)
        {
            mMainGenerator->tryToStop();
        }
    }

    void PowerSourceService::startApu()
    {
        if(mAuxPowerUnit != nullptr)
        {
            mAuxPowerUnit->tryToStart();
        }
    }

    void PowerSourceService::stopApu()
    {
        if(mAuxPowerUnit != nullptr)
        {
            mAuxPowerUnit->tryToStop();
        }
    }

    void PowerSourceService::setMainGeneratorRunning()
    {
        emit mainGeneratorStarted();
    }

    void PowerSourceService::setMainGeneratorStopped()
    {
        emit mainGeneratorStopped();
    }

    void PowerSourceService::setApuRunning()
    {
        emit apuGeneratorStarted();
    }

    void PowerSourceService::setApuStopped()
    {
        emit apuGeneratorStopped();
    }

    void PowerSourceService::setBatteryEstimatedTime(int aRestTime, int aPercent)
    {
        emit batteryEstimatedTimeChanged(aRestTime, aPercent);
    }

    void PowerSourceService::setSilentWatchTimes(QVector<int> aTimeList)
    {
        emit silentWatchTimesChanged(aTimeList);
    }

    void PowerSourceService::setPredictedTime(QVector<int> aTimeList)
    {
        emit predictedTimeChanged(aTimeList);
    }

    MainGenerator *PowerSourceService::getMainGenerator()
    {
        return mMainGenerator;
    }

    AuxPowerUnit *PowerSourceService::getAuxPowerUnit()
    {
        return mAuxPowerUnit;
    }

    Battery *PowerSourceService::getBattery()
    {
        return mBattery;
    }
}
