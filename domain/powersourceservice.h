#ifndef POWERSOURCESERVICE_H
#define POWERSOURCESERVICE_H

#include <QObject>

#include "powersimulator.h"
#include "batterysimulator.h"

namespace domain {

    class MainGenerator;
    class AuxPowerUnit;
    class Battery;

    class PowerSourceService : public QObject
    {
        Q_OBJECT

        friend class domain::PowerSimulator;
        friend class domain::BatterySimulator;

    public:

        enum class UserCommand
        {
            Undefined,
            Ok,
            Cancel,
            Turn_On,
            Turn_Off
        };

        enum PowerSourceState
        {
            Stopped = 1,
            Starting,
            Running,
            Stopping
        };

        explicit PowerSourceService(QObject *parent = nullptr);

        void setMainGenerator(MainGenerator* aMainGenerator);
        void setAuxPowerUnit(AuxPowerUnit* aAuxPowerUnit);
        void setBattery(Battery* aBattery);
        void connectComponents();

        void setPowerSourceCommand(bool isMainGen, UserCommand aCmd);

        void startMainGenerator();
        void stopMainGenerator();

        void startApu();
        void stopApu();

    public slots:
        void setMainGeneratorRunning();
        void setMainGeneratorStopped();
        void setApuRunning();
        void setApuStopped();
        void setBatteryEstimatedTime(int aRestTime, int aPercent);
        void setSilentWatchTimes(QVector<int> aTimeList);
        void setPredictedTime(QVector<int> aTimeList);

    private:
        MainGenerator* mMainGenerator;
        AuxPowerUnit* mAuxPowerUnit;
        Battery* mBattery;

        MainGenerator* getMainGenerator();
        AuxPowerUnit* getAuxPowerUnit();
        Battery* getBattery();

    signals:
        void mainGeneratorStarted();
        void mainGeneratorStopped();
        void mainGeneratorStateChanged(int aState);
        void apuStateChanged(int aState);
        void apuGeneratorStarted();
        void apuGeneratorStopped();
        void batteryEstimatedTimeChanged(int aAmountMinutes, int aPercent);
        void silentWatchTimesChanged(QVector<int> aTimeList);
        void predictedTimeChanged(QVector<int> aTimeList);

    };
}
#endif // POWERSOURCESERVICE_H
