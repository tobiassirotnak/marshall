#ifndef DEVICESIMULATOR_H
#define DEVICESIMULATOR_H

#include <QObject>
#include <QTimer>

namespace domain {

    class DeviceService;

    class DeviceSimulator : public QObject
    {
        Q_OBJECT
    public:
        explicit DeviceSimulator(QObject *parent = nullptr);

        void startActualValueSimulation(bool doStart);
        void synchronizeWithHardware(bool isMainGenOn, bool isApuOn);

        void setDeviceService(domain::DeviceService* aDeviceService);        

    public slots:
        void simulateActualDeviceValues();

    private:
        domain::DeviceService* mDeviceService;
        QTimer mActValSimTimer;

    signals:

    };
}
#endif // DEVICESIMULATOR_H
