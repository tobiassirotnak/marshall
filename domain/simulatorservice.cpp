#include <QDebug>
#include "simulatorservice.h"
#include "devicesimulator.h"
#include "powersimulator.h"
#include "batterysimulator.h"
#include "deviceservice.h"

namespace domain {

    SimulatorService::SimulatorService(QObject *parent) : QObject(parent),
        mDeviceSimulator(new DeviceSimulator(this)),
        mPowerSimulator(new PowerSimulator(this)),
        mBatterySimulator(new BatterySimulator(this)),
        mDeviceService(nullptr),
        mPowerSourceService(nullptr)
    {

    }

    void SimulatorService::setDeviceService(DeviceService *aDeviceService)
    {
        mDeviceService = aDeviceService;        
        mDeviceSimulator->setDeviceService(aDeviceService);
    }

    void SimulatorService::setPowerSourceService(PowerSourceService *aPowerSourceService)
    {
        mPowerSourceService = aPowerSourceService;
        mPowerSimulator->setPowerSourceService(aPowerSourceService);
        mBatterySimulator->setPowerSourceService(aPowerSourceService);
    }

    void SimulatorService::startActualValueSimulation(bool doStart)
    {
        mPowerSimulator->setBatteryStateOfCharge(85);
        mPowerSimulator->stopMainGenerator();
        mPowerSimulator->stopAuxPowerUnit();
        mDeviceSimulator->synchronizeWithHardware(false, false);
        mDeviceSimulator->startActualValueSimulation(doStart);
        startBatteryRestTimeSimulation(doStart);
    }

    void SimulatorService::setBatterySoC(int aValue)
    {
        mBatterySimulator->setBatterySoC(aValue);
    }

    void SimulatorService::startBatteryRestTimeSimulation(bool doStart)
    {
        mBatterySimulator->startBatteryRestTimeSimulation(doStart);
    }

    void SimulatorService::startMainGenerator()
    {
        mPowerSimulator->startMainGenerator();
    }

    void SimulatorService::stopMainGenerator()
    {
        mPowerSimulator->stopMainGenerator();
    }

    void SimulatorService::startAuxPowerUnit()
    {
        mPowerSimulator->startAuxPowerUnit();
    }

    void SimulatorService::stopAuxPowerUnit()
    {
        mPowerSimulator->stopAuxPowerUnit();
    }
}
