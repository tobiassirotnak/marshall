#include<QtDebug>

#include "devicesimulator.h"
#include "device.h"
#include "deviceservice.h"

namespace domain {

    DeviceSimulator::DeviceSimulator(QObject *parent) : QObject(parent),
        mDeviceService(nullptr),
        mActValSimTimer()
    {
        connect(&mActValSimTimer,&QTimer::timeout,this,&DeviceSimulator::simulateActualDeviceValues);
    }

    void DeviceSimulator::startActualValueSimulation(bool doStart)
    {
        if(doStart)
        {            
            mActValSimTimer.start(500);
        }
        else
        {
            mActValSimTimer.stop();
        }
    }

    void DeviceSimulator::synchronizeWithHardware(bool isMainGenOn, bool isApuOn)
    {
        if(mDeviceService)
        {
            QVector<Device *> lDeviceList = mDeviceService->getDeviceList();

            for(int i = 0; i < lDeviceList.size(); ++i)
            {
                lDeviceList.at(i)->checkEnergyLimits(isMainGenOn, isApuOn);
            }
        }
    }

    void DeviceSimulator::setDeviceService(DeviceService *aDeviceService)
    {
        mDeviceService = aDeviceService;
    }

    void DeviceSimulator::simulateActualDeviceValues()
    {
        static int lRestTime = 100;

        QVector<Device *> lDeviceList = mDeviceService->getDeviceList();
        for(int i = 0; i < lDeviceList.size(); ++i)
        {
            int lRange = (lDeviceList.at(i)->getMaxValue() - lDeviceList.at(i)->getMidValue()) / 8;
            if(lRange <= 0)
            {
                lRange = 1;
            }
            int lRandomValue = rand() % (lRange * 2) + 1;
            int lCalcVal = lDeviceList.at(i)->getMidValue() - lRange + lRandomValue;
            if(lCalcVal <= 0)
            {
                lCalcVal = 1;
            }
            if(!lDeviceList.at(i)->isRunning())
            {
                lCalcVal = 0;
            }
            lDeviceList.at(i)->setActualValue(lCalcVal);
            lDeviceList.at(i)->setMidValue(lDeviceList.at(i)->getMidValue());

            lDeviceList.at(i)->setRestTime(lRestTime * (i + 1));
        }
        --lRestTime;
    }
}
