#include <QDebug>

#include "device.h"

namespace domain {

    Device::Device(int aId, QObject *parent) : QObject(parent),
        mId(aId),
        mName(),
        mDeviceFSM(this),
        mCommandSubState(DeviceService::CommandSubState::Off),
        mActualValue(0),
        mMaxValue(100),
        mMidValue(50),
        mAvailableBatteryCharge(0)
    {        
    }

    DeviceDescriptor Device::getDescriptor()
    {
        DeviceDescriptor lDescriptor;
        lDescriptor.Name = mName;
        lDescriptor.CommandState = static_cast<int>(mCommandSubState);
        lDescriptor.MidValue = getCalculatedValue(mMidValue);
        return lDescriptor;
    }

    void Device::initFSM()
    {
        mDeviceFSM.initStates();
        connect(&mDeviceFSM,&DeviceFSM::stateChanged,this,&Device::setState);
    }

    bool Device::isRunning()
    {        
        return mDeviceFSM.isRunning();
    }

    bool Device::setUserCommand(DeviceService::UserCommand aCmd)
    {
        bool lMustChangeVisualisation = true;

        if(mDeviceFSM.isOnNormal())
        {
            lMustChangeVisualisation = setRunningNormalCommand(aCmd);
        }
        else if(mDeviceFSM.isOnLow())
        {
            lMustChangeVisualisation = setRunningLowPowerCommand(aCmd);
        }
        else if(mDeviceFSM.isDisabledLow())
        {
            lMustChangeVisualisation = setDisabledLowPowerCommand(aCmd);
        }
        else if(mDeviceFSM.isDisabledOverload())
        {
            lMustChangeVisualisation = setDisabledOverloadCommand(aCmd);
        }
        else if(mDeviceFSM.isDisabledNoPower())
        {
            lMustChangeVisualisation = setDisabledMissingPowerCommand(aCmd);
        }
        else if(mDeviceFSM.isOff())
        {
            lMustChangeVisualisation = setRunningNormalCommand(aCmd);
        }
        else
        {
            qDebug() << "Device::setUserCommand:" << QString::number(static_cast<int>(aCmd)) << " could not be executed";
        }

        return lMustChangeVisualisation;
    }

    void Device::setProfileStates(data::DeviceStateDescriptor aProfileStateDescr)
    {
        mEnergyLimits = aProfileStateDescr;
    }

    data::DeviceStateDescriptor Device::getProfileStates()
    {
        return mEnergyLimits;
    }

    void Device::setName(QString aName)
    {
        mName = aName;
    }

    void Device::setActualValue(int aValue)
    {
        mActualValue = aValue;
        emit actualValueChanged(mId, getCalculatedValue(mActualValue), mActualValue);        
    }

    void Device::setRestTime(int aValue)
    {
        emit restTimeChanged(mId, aValue);
    }

    void Device::setMidValue(int aValue)
    {
        mMidValue = aValue;
        emit midValueChanged(mId, getCalculatedValue(mMidValue));
    }

    void Device::setMaxValue(int aValue)
    {
        mMaxValue = aValue;
        mMidValue = mMaxValue * 80 / 100;
    }

    void Device::setBatteryStateOfCharge(int aValue, bool isMainGenOn, bool isApuOn)
    {
        mAvailableBatteryCharge = aValue;
        checkEnergyLimits(isMainGenOn, isApuOn);
    }

    void Device::setEnergyLimits(data::DeviceStateDescriptor aLimit)
    {
        mEnergyLimits = aLimit;
    }

    void Device::setState(DeviceService::CommandSubState aCmdSubState)
    {
        //qDebug() << "Device:" << mId << ", state:" << static_cast<int>(aState) << ", aCmdSubState:" << static_cast<int>(aCmdSubState);
        mCommandSubState = aCmdSubState;
        emit stateChanged(mId, getDescriptor());
    }

    void Device::checkSingleSocLimite(data::DeviceState aLimitState)
    {
        if(aLimitState == data::DeviceState::On)
        {
            qDebug() << "Device Nr" << mId << " emit switchSocFull";
            emit mDeviceFSM.switchSocNormal();
        }
        else if(aLimitState == data::DeviceState::ActionRequired)
        {
            qDebug() << "Device Nr" << mId << " emit switchSocLow";
            emit mDeviceFSM.switchSocLow();
        }
        else if(aLimitState == data::DeviceState::Override)
        {
            qDebug() << "Device Nr" << mId << " emit switchSocOverload";
            emit mDeviceFSM.switchSocOverload();
        }
        else
        {
            qDebug() << "Device Nr" << mId << " emit switchSocOff";
            emit mDeviceFSM.switchSocOff();
        }
    }

    int Device::getCalculatedValue(int aValue)
    {
        int lMaxVal = mMaxValue;
        if(lMaxVal < 1)
        {
            lMaxVal = 1;
        }
        return (100 * aValue) / lMaxVal;
    }

    int Device::getMidValue()
    {
        return mMidValue;
    }

    int Device::getMaxValue()
    {
        return mMaxValue;
    }

    int Device::getActualValue()
    {
        return mActualValue;
    }

    void Device::checkEnergyLimits(bool isMainGenOn, bool isApuOn)
    {
        if(isMainGenOn || isApuOn)
        {
            if(isMainGenOn && (mEnergyLimits.MainGen == data::DeviceState::On))
            {
                emit mDeviceFSM.switchSocNormal();
            }
            else if(isApuOn && (mEnergyLimits.APU == data::DeviceState::On))
            {
                emit mDeviceFSM.switchSocNormal();
            }
        }
        else
        {
            if(mAvailableBatteryCharge == 100)
            {
                emit mDeviceFSM.switchSocNormal();
            }
            else if(mAvailableBatteryCharge >= 80)
            {
                checkSingleSocLimite(mEnergyLimits.LT100);
            }
            else if(mAvailableBatteryCharge >= 60)
            {
                checkSingleSocLimite(mEnergyLimits.LT80);
            }
            else if(mAvailableBatteryCharge >= 40)
            {
                checkSingleSocLimite(mEnergyLimits.LT60);
            }
            else if(mAvailableBatteryCharge >= 20)
            {
                checkSingleSocLimite(mEnergyLimits.LT40);
            }
            else
            {
                checkSingleSocLimite(mEnergyLimits.LT20);
            }
        }
    }

    bool Device::setRunningNormalCommand(DeviceService::UserCommand aCmd)
    {
        bool lMustChangeVisualisation = true;
        switch(aCmd)
        {
            case DeviceService::UserCommand::Turn_Off:
            {
                mCommandSubState = DeviceService::CommandSubState::AskForTurningOff;                
                break;
            }
            case DeviceService::UserCommand::Ok:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForTurningOff)
                {
                    mCommandSubState = DeviceService::CommandSubState::WaitForOff;
                    emit mDeviceFSM.switchOperationOff();
                }
                else
                {
                    mCommandSubState = DeviceService::CommandSubState::WaitForRunning;
                    emit mDeviceFSM.switchOperationOn();
                }
                emit profileStateChanged();
                break;
            }
            case DeviceService::UserCommand::Cancel:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForTurningOff)
                {
                    mCommandSubState = DeviceService::CommandSubState::Running;
                }
                else
                {
                    mCommandSubState = DeviceService::CommandSubState::Off;
                    //emit mDeviceFSM.switchOperationOff();
                }
                break;
            }
            case DeviceService::UserCommand::Turn_On:
            {
                mCommandSubState = DeviceService::CommandSubState::AskForTurningOn;
                break;
            }
            default:
            {
                lMustChangeVisualisation = false;
                qDebug() << "------ Unhandled Command:" << static_cast<int>(aCmd);
                break;
            }
        }

        return lMustChangeVisualisation;
    }

    bool Device::setRunningLowPowerCommand(DeviceService::UserCommand aCmd)
    {
        bool lCouldSetCmd = true;
        switch(aCmd)
        {
            case DeviceService::UserCommand::Action_Required:
        case DeviceService::UserCommand::Turn_On:
            {
                mCommandSubState = DeviceService::CommandSubState::AskForAction;
                break;
            }
            case DeviceService::UserCommand::Turn_Off:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForAction)
                {
                    mCommandSubState = DeviceService::CommandSubState::WaitForOff;
                    emit mDeviceFSM.switchOperationOff();
                }
                break;
            }
            case DeviceService::UserCommand::Override:
            {
                mCommandSubState = DeviceService::CommandSubState::AskForOverride;
                break;
            }
            case DeviceService::UserCommand::Ok:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForOverride)
                {
                    mCommandSubState = DeviceService::CommandSubState::WaitForRunning;
                    emit mDeviceFSM.switchOperationOn();
                }
                break;
            }
            case DeviceService::UserCommand::Cancel:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForOverride)
                {
                    mCommandSubState = DeviceService::CommandSubState::RunningLow;
                }
                break;
            }
            default:
            {
                lCouldSetCmd = false;
                qDebug() << "------ Unhandled Command:" << static_cast<int>(aCmd);
                break;
            }
        }

        return lCouldSetCmd;
    }

    bool Device::setDisabledOverloadCommand(DeviceService::UserCommand aCmd)
    {
        bool lCouldSetCmd = true;
        switch(aCmd)
        {
            case DeviceService::UserCommand::Reset:
            {
                mCommandSubState = DeviceService::CommandSubState::AskForReset;
                break;
            }
            case DeviceService::UserCommand::Ok:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForReset)
                {
                    mCommandSubState = DeviceService::CommandSubState::WaitForDisable;
                    emit mDeviceFSM.switchOperationDisabled();
                }
                break;
            }
            case DeviceService::UserCommand::Cancel:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForReset)
                {
                    mCommandSubState = DeviceService::CommandSubState::Disabled;
                }
                break;
            }
            default:
            {
                lCouldSetCmd = false;
                qDebug() << "------ Unhandled Command:" << static_cast<int>(aCmd);
                break;
            }
        }

        return lCouldSetCmd;
    }

    bool Device::setDisabledLowPowerCommand(DeviceService::UserCommand aCmd)
    {
        bool lCouldSetCmd = true;
        switch(aCmd)
        {
            case DeviceService::UserCommand::Override:
            {
                mCommandSubState = DeviceService::CommandSubState::AskForOverride;
                break;
            }
            case DeviceService::UserCommand::Ok:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForOverride)
                {
                    mCommandSubState = DeviceService::CommandSubState::WaitForOverride;
                    emit mDeviceFSM.switchOperationOff();
                }
                break;
            }
            case DeviceService::UserCommand::Cancel:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForOverride)
                {
                    mCommandSubState = DeviceService::CommandSubState::Override; // FIXME: Look, here was Disabled
                }
                break;
            }
            case DeviceService::UserCommand::Turn_On:
            {
                mCommandSubState = DeviceService::CommandSubState::AskForTurningOn;
                break;
            }
            default:
            {
                lCouldSetCmd = false;
                qDebug() << "------ Unhandled Command:" << static_cast<int>(aCmd);
                break;
            }
        }

        return lCouldSetCmd;
    }

    bool Device::setDisabledMissingPowerCommand(DeviceService::UserCommand aCmd)
    {
        bool lCouldSetCmd = true;
        switch(aCmd)
        {
            case DeviceService::UserCommand::Add_Source:
            {
                mCommandSubState = DeviceService::CommandSubState::AskForSource;
                break;
            }
            case DeviceService::UserCommand::Cancel:
            {
                if(mCommandSubState == DeviceService::CommandSubState::AskForSource)
                {
                    mCommandSubState = DeviceService::CommandSubState::Disabled;
                }
                break;
            }
            case DeviceService::UserCommand::Turn_On:
            {
            mCommandSubState = DeviceService::CommandSubState::WaitForOff;
            emit mDeviceFSM.switchOperationOff();
                break;
            }

            default:
            {
                lCouldSetCmd = false;
                qDebug() << "------ Unhandled Command:" << static_cast<int>(aCmd);
                break;
            }
        }

        return lCouldSetCmd;
    }
}

