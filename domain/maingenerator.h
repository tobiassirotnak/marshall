#ifndef MAINGENERATOR_H
#define MAINGENERATOR_H

#include <QObject>
#include <QTimer>
#include "powersourceservice.h"

namespace domain {

    class MainGenerator : public QObject
    {
        Q_OBJECT
    public:

        explicit MainGenerator(QObject *parent = nullptr);

        void tryToStart();
        void tryToStop();
        bool isRunning();

        void setState(PowerSourceService::PowerSourceState aState);
        bool setUserCommand(PowerSourceService::UserCommand aCmd);

    private:
        QTimer mStateChangeDuranceTimer;
        PowerSourceService::PowerSourceState mState;

        void setStateChangeTimeout();

    private slots:
        void sendSimulatedStarted();
        void sendSimulatedStopped();


    signals:
        void running();
        void stopped();
        void stateChanged(PowerSourceService::PowerSourceState aState);

        void simulatedRunning();
        void simulatedStopped();

    };
}

#endif // MAINGENERATOR_H
