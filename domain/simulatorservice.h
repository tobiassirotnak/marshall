#ifndef SIMULATORSERVICE_H
#define SIMULATORSERVICE_H

#include <QObject>

namespace domain {

    class DeviceSimulator;
    class PowerSimulator;
    class BatterySimulator;
    class DeviceService;
    class PowerSourceService;

    class SimulatorService : public QObject
    {
        Q_OBJECT
    public:
        explicit SimulatorService(QObject *parent = nullptr);

        void setDeviceService(domain::DeviceService* aDeviceService);
        void setPowerSourceService(domain::PowerSourceService* aPowerSourceService);

        void startActualValueSimulation(bool doStart);
        void setBatterySoC(int aValue);

        void startBatteryRestTimeSimulation(bool doStart);

        void startMainGenerator();
        void stopMainGenerator();
        void startAuxPowerUnit();
        void stopAuxPowerUnit();

    private:
        domain::DeviceSimulator* mDeviceSimulator;
        domain::PowerSimulator* mPowerSimulator;
        domain::BatterySimulator* mBatterySimulator;
        domain::DeviceService* mDeviceService;
        domain::PowerSourceService* mPowerSourceService;

    signals:

    };
}

#endif // SIMULATORSERVICE_H
