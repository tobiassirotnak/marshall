#include "batterysimulator.h"
#include "powersourceservice.h"
#include "battery.h"

namespace domain {

    BatterySimulator::BatterySimulator(QObject *parent) : QObject(parent),
        mPowerSourceService(nullptr),
        mRestTimeUpdateTimer()
    {
        mRestTimeUpdateTimer.setSingleShot(false);
        connect(&mRestTimeUpdateTimer,&QTimer::timeout,this,&BatterySimulator::simulateRestTime);
    }

    void BatterySimulator::setPowerSourceService(PowerSourceService *aPowerSourceService)
    {
        mPowerSourceService = aPowerSourceService;
    }

    void BatterySimulator::startBatteryRestTimeSimulation(bool doStart)
    {
        if(doStart)
        {
            mRestTimeUpdateTimer.start(1400);
        }
        else
        {
            mRestTimeUpdateTimer.stop();
        }
    }

    void BatterySimulator::setBatterySoC(int aValue)
    {
        if(mPowerSourceService != nullptr)
        {
            mPowerSourceService->getBattery()->setStateOfCharge(aValue);
        }
    }

    void BatterySimulator::simulateRestTime()
    {
        if(mPowerSourceService != nullptr)
        {
            int lSoC = mPowerSourceService->getBattery()->getStateOfCharge();
            if(lSoC > 10)
            {
                lSoC -= 1;
            }            
            setBatterySoC(lSoC);
        }
    }
}
