#include <QDebug>

#include "device.h"
#include "deviceservice.h"
#include "energymgt.h"

namespace domain {

    DeviceService::DeviceService(QObject *parent) : QObject(parent),
        mDeviceList(),
        mProfileList(),
        mSelectedProfile(domain::DeviceService::Standard),
        mConfigurationService(nullptr)
    {

    }

    void DeviceService::loadDevices()
    {
        if(mConfigurationService)
        {
            QVector<data::DeviceDescriptor> lDeviceDescrList = mConfigurationService->getDeviceList();

            for(int i = 0; i < lDeviceDescrList.size(); ++i)
            {
                data::DeviceDescriptor lDescr = lDeviceDescrList.at(i);

                Device *lDevice = new Device(i + 1);
                lDevice->setName(lDescr.Name);
                lDevice->setMaxValue(lDescr.MaxValue);
                lDevice->setEnergyLimits(lDescr.EnergyLimits);
                lDevice->initFSM();

                mDeviceList.append(lDevice);
                connect(lDevice,&Device::actualValueChanged,this,&DeviceService::setActualValue);
                connect(lDevice,&Device::midValueChanged,this,&DeviceService::setMidValue);
                connect(lDevice,&Device::stateChanged,this,&DeviceService::deviceStateChanged);
                connect(lDevice,&Device::restTimeChanged,this,&DeviceService::setRestTime);
                connect(lDevice,&Device::profileStateChanged,this,&DeviceService::setToCustomProfile);
            }
        }
    }

    void DeviceService::loadProfiles()
    {
        mProfileList.append(mConfigurationService->getDefaultCustomProfile());
        mProfileList.append(mConfigurationService->getStandardProfile());
        mProfileList.append(mConfigurationService->getSilentWatchProfile());
        mProfileList.append(mConfigurationService->getPro3Profile());
    }

    void DeviceService::startupSystem()
    {
        emit switchToProfile(static_cast<int>(DeviceProfile::Standard));
    }

    QVector<DeviceDescriptor> DeviceService::getDeviceDescr(int aProfileNr)
    {
        QVector<DeviceDescriptor> lDescrList;

        setProfile(aProfileNr);         

        for(int i = 0; i < mDeviceList.size(); ++i)
        {
            lDescrList.append(mDeviceList.at(i)->getDescriptor());
        }

        return lDescrList;
    }

    void DeviceService::setDeviceCommand(int aDeviceNr, DeviceService::UserCommand aCmd)
    {
        int lIndex = aDeviceNr -1;
        if((lIndex >= 0) && (lIndex < mDeviceList.size()))
        {
            Device *lDevice = mDeviceList.at(lIndex);
            if(lDevice->setUserCommand(aCmd))
            {
                emit deviceStateChanged(aDeviceNr, lDevice->getDescriptor());
            }
        }
    }

    void DeviceService::setProfile(int aProfileNr)
    {
        int lProfileIndex = aProfileNr - 1;
        if(lProfileIndex < 0)
        {
            lProfileIndex = 0;
        }
        if(mProfileList.size() > lProfileIndex)
        {
            QVector<data::DeviceStateDescriptor> lProfile = mProfileList.at(lProfileIndex);
            if(lProfile.size() == mDeviceList.size())
            {
                for(int i = 0; i < lProfile.size(); ++i)
                {
                    mDeviceList[i]->setProfileStates(lProfile.at(i));
                }
            }
            emit profileChanged();
        }
    }

    DeviceService::DeviceProfile DeviceService::getSelectedProfile()
    {
        return mSelectedProfile;
    }

    void DeviceService::setConfigurationService(data::ConfigurationService *aConfigurationService)
    {
        mConfigurationService = aConfigurationService;
    }

    void DeviceService::setActualValue(int aDeviceNr, int aCalcValue, int aRealValue)
    {
        if(mDeviceList.at(aDeviceNr - 1)->isRunning())
        {
            emit actualValueChanged(aDeviceNr, aCalcValue, aRealValue);
        }
    }

    void DeviceService::setBatteryStateOfCharge(int aValue, bool isMainGenOn, bool isApuOn)
    {
        for(int i = 0; i < mDeviceList.size(); ++i)
        {
            mDeviceList.at(i)->setBatteryStateOfCharge(aValue, isMainGenOn, isApuOn);
        }
    }

    void DeviceService::setMidValue(int aDeviceNr, int aValue)
    {
        emit midValueChanged(aDeviceNr,aValue);
    }

    void DeviceService::setRestTime(int aDeviceNr, int aRestTime)
    {
        emit restTimeChanged(aDeviceNr,aRestTime);
    }

    void DeviceService::setToCustomProfile()
    {
        qDebug() << "copy actual Profile to Custom Profile and switch to it";
        int lCustomProfileIndex = DeviceProfile::Custom - 1;
        mProfileList.replace(lCustomProfileIndex, cloneDeviceListProfileData());

        emit switchToProfile(static_cast<int>(DeviceProfile::Custom));
    }

    QVector<Device *> DeviceService::getDeviceList()
    {
        return mDeviceList;
    }

    QVector<data::DeviceStateDescriptor>  DeviceService::cloneDeviceListProfileData()
    {
        QVector<data::DeviceStateDescriptor> lProfile;
        for(int i = 0; i < mDeviceList.size(); ++i)
        {
            lProfile.append( mDeviceList.at(i)->getProfileStates());
        }

        return lProfile;
    }
}
