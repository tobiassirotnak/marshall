#ifndef BATTERYSIMULATOR_H
#define BATTERYSIMULATOR_H

#include <QObject>
#include <QTimer>

namespace domain {

    class PowerSourceService;

    class BatterySimulator : public QObject
    {
        Q_OBJECT
    public:
        explicit BatterySimulator(QObject *parent = nullptr);

        void setPowerSourceService(PowerSourceService *aPowerSourceService);

        void startBatteryRestTimeSimulation(bool doStart);
        void setBatterySoC(int aValue);

    private:
        domain::PowerSourceService* mPowerSourceService;
        QTimer mRestTimeUpdateTimer;

    private slots:
        void simulateRestTime();

    signals:

    };
}

#endif // BATTERYSIMULATOR_H
