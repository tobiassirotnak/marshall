#include <QDebug>
#include "devicefsm.h"

namespace domain {

    DeviceFSM::DeviceFSM(QObject *parent) : QObject(parent),
        mFSM(this)
    {

    }

    void DeviceFSM::initStates()
    {
        mInitialized = new QState(QState::ParallelStates);
        mOperatingState = new QState(mInitialized);
        mStateOfChargeLevel = new QState(mInitialized);

        mOn = new QState(mOperatingState);
        QObject::connect(mOn, &QState::entered, this, &DeviceFSM::onEnteredOn);
        mOff = new QState(mOperatingState);
        QObject::connect(mOff, &QState::entered, this, &DeviceFSM::onEnteredOff);
        mDisabled = new QState(mOperatingState);
        QObject::connect(mDisabled, &QState::entered, this, &DeviceFSM::onEnteredDisabled);
        mFromOffToOn = new QState(mOperatingState);
        QObject::connect(mFromOffToOn, &QState::entered, this, &DeviceFSM::onEnteredFromOffToOn);
        mFromOffToDisabled = new QState(mOperatingState);
        QObject::connect(mFromOffToDisabled, &QState::entered, this, &DeviceFSM::onEnteredFromOffToDisabled);
        mFromOnToOff = new QState(mOperatingState);
        QObject::connect(mFromOnToOff, &QState::entered, this, &DeviceFSM::onEnteredFromOnToOff);
        mFromOnToDisabled = new QState(mOperatingState);
        QObject::connect(mFromOnToDisabled, &QState::entered, this, &DeviceFSM::onEnteredFromOnToDisabled);
        mFromDisabledToOff = new QState(mOperatingState);
        QObject::connect(mFromDisabledToOff, &QState::entered, this, &DeviceFSM::onEnteredFromDisabledToOff);
        mFromDisabledToOn = new QState(mOperatingState);
        QObject::connect(mFromDisabledToOn, &QState::entered, this, &DeviceFSM::onEnteredFromDisabledToOn);

        mOperatingState->setInitialState(mOff);

        mSocNormal = new QState(mStateOfChargeLevel);
        QObject::connect(mSocNormal, &QState::entered, this, &DeviceFSM::onEnteredSocNormal);
        mSocLow = new QState(mStateOfChargeLevel);
        QObject::connect(mSocLow, &QState::entered, this, &DeviceFSM::onEnteredSocLow);
        mSocOff = new QState(mStateOfChargeLevel);
        QObject::connect(mSocOff, &QState::entered, this, &DeviceFSM::onEnteredSocOff);
        mSocOverload = new QState(mStateOfChargeLevel);
        QObject::connect(mSocOverload, &QState::entered, this, &DeviceFSM::onEnteredSocOverload);

        mStateOfChargeLevel->setInitialState(mSocOff);

        mFSM.addState(mInitialized);
        mFSM.setInitialState(mInitialized);

        // OperatingState
        // On To Off
        mOn->addTransition(this, &DeviceFSM::switchOperationOff, mFromOnToOff);
        mFromOnToOff->addTransition(this, &DeviceFSM::switchOperationOffDone, mOff);
        mFromOnToOff->addTransition(this, &DeviceFSM::switchOperationCancel, mOn);

        // On to Disabled
        mOn->addTransition(this, &DeviceFSM::switchOperationDisabled, mFromOnToDisabled);
        mFromOnToDisabled->addTransition(this, &DeviceFSM::switchOperationDisabledDone, mDisabled);
        mFromOnToDisabled->addTransition(this, &DeviceFSM::switchOperationCancel, mOn);

        // Off To On
        mOff->addTransition(this, &DeviceFSM::switchOperationOn, mFromOffToOn);
        mFromOffToOn->addTransition(this, &DeviceFSM::switchOperationOnDone, mOn);
        mFromOffToOn->addTransition(this, &DeviceFSM::switchOperationCancel, mOff);

        // Off To Disabled
        mOff->addTransition(this, &DeviceFSM::switchOperationDisabled, mFromOffToDisabled);
        mFromOffToDisabled->addTransition(this, &DeviceFSM::switchOperationDisabledDone, mDisabled);
        mFromOffToDisabled->addTransition(this, &DeviceFSM::switchOperationCancel, mOff);

        // Disabled to On
        mDisabled->addTransition(this, &DeviceFSM::switchOperationOn, mFromDisabledToOn);
        mFromDisabledToOn->addTransition(this, &DeviceFSM::switchOperationOnDone, mOn);
        mFromDisabledToOn->addTransition(this, &DeviceFSM::switchOperationCancel, mDisabled);

        // Disabled to Off
        mDisabled->addTransition(this, &DeviceFSM::switchOperationOff, mFromDisabledToOff);
        mFromDisabledToOff->addTransition(this, &DeviceFSM::switchOperationOffDone, mOff);
        mFromDisabledToOff->addTransition(this, &DeviceFSM::switchOperationCancel, mDisabled);

        // StateOfChargeLevel
        mSocOff->addTransition(this, &DeviceFSM::switchSocNormal, mSocNormal);
        mSocOff->addTransition(this, &DeviceFSM::switchSocLow, mSocLow);
        mSocOff->addTransition(this, &DeviceFSM::switchSocOverload, mSocOverload);

        mSocNormal->addTransition(this, &DeviceFSM::switchSocOff, mSocOff);
        mSocNormal->addTransition(this, &DeviceFSM::switchSocLow, mSocLow);
        mSocNormal->addTransition(this, &DeviceFSM::switchSocOverload, mSocOverload);

        mSocLow->addTransition(this, &DeviceFSM::switchSocNormal, mSocNormal);
        mSocLow->addTransition(this, &DeviceFSM::switchSocOff, mSocOff);
        mSocLow->addTransition(this, &DeviceFSM::switchSocOverload, mSocOverload);

        mSocOverload->addTransition(this, &DeviceFSM::switchSocNormal, mSocNormal);
        mSocOverload->addTransition(this, &DeviceFSM::switchSocLow, mSocLow);
        mSocOverload->addTransition(this, &DeviceFSM::switchSocOff, mSocOff);

        mFSM.start();
    }

    bool DeviceFSM::isRunning()
    {
        bool lIsRunning = false;
        if((mFSM.configuration().contains(mOn)) ||
           (mFSM.configuration().contains(mFromOnToOff)) ||
           (mFSM.configuration().contains(mFromOnToDisabled)))
        {
            lIsRunning = true;
        }

        return lIsRunning;
    }

    bool DeviceFSM::isOnNormal()
    {
        bool lIsOnFull = false;
        if((mFSM.configuration().contains(mOn)) &&
           (mFSM.configuration().contains(mSocNormal)))
        {
            lIsOnFull = true;
        }

        return lIsOnFull;
    }

    bool DeviceFSM::isOnLow()
    {
        bool lIsOnLow = false;
        if((mFSM.configuration().contains(mOn)) &&
           (mFSM.configuration().contains(mSocLow)))
        {
            lIsOnLow = true;
        }

        return lIsOnLow;
    }

    bool DeviceFSM::isDisabledLow()
    {
        bool lIsDisabledLow = false;
        if((mFSM.configuration().contains(mDisabled)) &&
           (mFSM.configuration().contains(mSocLow)))
        {
            lIsDisabledLow = true;
        }

        return lIsDisabledLow;
    }

    bool DeviceFSM::isDisabledOverload()
    {
        bool lIsDisabledOverload = false;
        if((mFSM.configuration().contains(mDisabled)) &&
           (mFSM.configuration().contains(mSocOverload)))
        {
            lIsDisabledOverload = true;
        }

        return lIsDisabledOverload;
    }

    bool DeviceFSM::isDisabledNoPower()
    {
        bool lIsDisabledNoEnergy = false;
        if((mFSM.configuration().contains(mDisabled)) &&
           (mFSM.configuration().contains(mSocOff)))
        {
            lIsDisabledNoEnergy = true;
        }

        return lIsDisabledNoEnergy;
    }

    bool DeviceFSM::isOff()
    {
        bool lIsOff = false;
        if(mFSM.configuration().contains(mOff))
        {
            lIsOff = true;
        }

        return lIsOff;
    }

    DeviceService::DeviceProfileState DeviceFSM::getStateOfChargeLevel()
    {
        DeviceService::DeviceProfileState lState = DeviceService::DeviceProfileState::Running_Normal;
        if(mFSM.configuration().contains(mSocLow))
        {
            lState = DeviceService::DeviceProfileState::Running_Power_Low;
        }
        else if(mFSM.configuration().contains(mSocOverload))
        {
            lState = DeviceService::DeviceProfileState::Disabled_Overload;
        }
        else if(mFSM.configuration().contains(mSocOff))
        {
            lState = DeviceService::DeviceProfileState::Off;
        }
        return lState;
    }

    DeviceService::CommandSubState DeviceFSM::getCommandSubState()
    {
        DeviceService::CommandSubState lCmdSubState = DeviceService::CommandSubState::Off;
        if(mFSM.configuration().contains(mOn))
        {
            lCmdSubState = DeviceService::CommandSubState::Running;
            if(getStateOfChargeLevel() == DeviceService::DeviceProfileState::Running_Power_Low)
            {
                lCmdSubState = DeviceService::CommandSubState::RunningLow;
            }
        }
        else if(mFSM.configuration().contains(mOff))
        {
            lCmdSubState = DeviceService::CommandSubState::Off;
        }

        return lCmdSubState;
    }

    void DeviceFSM::onEnteredOn()
    {
        qDebug() << "onEnteredOn";

        emit stateChanged(DeviceService::CommandSubState::Running);
    }

    void DeviceFSM::onEnteredFromOnToOff()
    {
        qDebug() << "onEnteredFromOnToOff";
        // just for test:
        QTimer::singleShot(1000,[this] () {
            emit switchOperationOffDone();
        });
    }

    void DeviceFSM::onEnteredOff()
    {
        qDebug() << "onEnteredOff";
        emit stateChanged(DeviceService::CommandSubState::Off);
    }

    void DeviceFSM::onEnteredFromOnToDisabled()
    {
        qDebug() << "onEnteredFromOnToDisabled";
        // just for test:
        QTimer::singleShot(1000,[this] () {
            emit switchOperationDisabledDone();
        });
    }

    void DeviceFSM::onEnteredDisabled()
    {
        qDebug() << "onEnteredDisabled";
    }

    void DeviceFSM::onEnteredFromOffToOn()
    {
        qDebug() << "onEnteredFromOffToOn";

        // just for test:
        QTimer::singleShot(1000,[this] () {
            emit switchOperationOnDone();
        });
    }

    void DeviceFSM::onEnteredFromOffToDisabled()
    {
        qDebug() << "onEnteredFromOffToDisabled";
        // just for test:
        QTimer::singleShot(1000,[this] () {
            emit switchOperationDisabledDone();
        });
    }

    void DeviceFSM::onEnteredFromDisabledToOff()
    {
        qDebug() << "onEnteredFromDisabledToOff";
        // just for test:
        QTimer::singleShot(1000,[this] () {
            emit switchOperationOffDone();
        });
    }

    void DeviceFSM::onEnteredFromDisabledToOn()
    {
        qDebug() << "onEnteredFromDisabledToOn";
        // just for test:
        QTimer::singleShot(1000,[this] () {
            emit switchOperationOnDone();
        });
    }

    void DeviceFSM::onEnteredSocNormal()
    {
        qDebug() << "onEnteredSocFull:" << static_cast<int>(getStateOfChargeLevel());
        emit switchOperationOn();
    }

    void DeviceFSM::onEnteredSocLow()
    {
        qDebug() << "onEnteredSocLow";
        emit stateChanged(DeviceService::CommandSubState::RunningLow);
    }

    void DeviceFSM::onEnteredSocOff()
    {
        qDebug() << "onEnteredSocOff";
        emit switchOperationOff();
    }

    void DeviceFSM::onEnteredSocOverload()
    {
        qDebug() << "onEnteredSocOverload";
        emit switchOperationDisabled();
    }
}
