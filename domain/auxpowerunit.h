#ifndef AUXPOWERUNIT_H
#define AUXPOWERUNIT_H

#include <QObject>
#include <QTimer>
#include "powersourceservice.h"

namespace domain {

    class AuxPowerUnit : public QObject
    {
        Q_OBJECT
    public:

        explicit AuxPowerUnit(QObject *parent = nullptr);

        void tryToStart();
        void tryToStop();
        bool isRunning();

        void setState(PowerSourceService::PowerSourceState aState);
        bool setUserCommand(PowerSourceService::UserCommand aCmd);

    private:
        QTimer mStateChangeDuranceTimer;
        PowerSourceService::PowerSourceState mState;

        void setStateChangeTimeout();

    private slots:
        void sendSimulatedStarted();
        void sendSimulatedStopped();

    signals:
        void running();
        void stopped();
        void stateChanged(PowerSourceService::PowerSourceState aState);

        void simulatedRunning();
        void simulatedStopped();
    };
}

#endif // AUXPOWERUNIT_H
