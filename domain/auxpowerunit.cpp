#include "auxpowerunit.h"


namespace domain {

    const int cStartStopTimeout = 4000;

    AuxPowerUnit::AuxPowerUnit(QObject *parent) : QObject(parent),
        mStateChangeDuranceTimer(),
        mState(PowerSourceService::PowerSourceState::Stopped)
    {
        mStateChangeDuranceTimer.setSingleShot(true);
        connect(&mStateChangeDuranceTimer,&QTimer::timeout,this,&AuxPowerUnit::setStateChangeTimeout);

        connect(this,&AuxPowerUnit::simulatedRunning,this,&AuxPowerUnit::sendSimulatedStarted);
        connect(this,&AuxPowerUnit::simulatedStopped,this,&AuxPowerUnit::sendSimulatedStopped);
    }

    void AuxPowerUnit::tryToStart()
    {
        setState(PowerSourceService::PowerSourceState::Starting);
        mStateChangeDuranceTimer.start(cStartStopTimeout);        
    }

    void AuxPowerUnit::tryToStop()
    {
        setState(PowerSourceService::PowerSourceState::Stopping);
        mStateChangeDuranceTimer.start(cStartStopTimeout);
    }

    bool AuxPowerUnit::isRunning()
    {
        bool lIsRunning = false;
        if(mState == PowerSourceService::PowerSourceState::Running)
        {
            lIsRunning = true;
        }
        return lIsRunning;
    }

    void AuxPowerUnit::setState(PowerSourceService::PowerSourceState aState)
    {
        mState = aState;
        if(mState == PowerSourceService::PowerSourceState::Stopped)
        {
            mStateChangeDuranceTimer.stop();
            emit stopped();
        }
        else if(mState == PowerSourceService::PowerSourceState::Running)
        {
            mStateChangeDuranceTimer.stop();
            emit running();
        }
        emit stateChanged(mState);
    }

    bool AuxPowerUnit::setUserCommand(PowerSourceService::UserCommand aCmd)
        {
            if(aCmd == PowerSourceService::UserCommand::Ok)
            {
                if(mState == PowerSourceService::PowerSourceState::Stopped)
                {
                    tryToStart();
                }
                else if(mState == PowerSourceService::PowerSourceState::Running)
                {
                    tryToStop();
                }
            }
            else if(aCmd == PowerSourceService::UserCommand::Cancel)
            {
                if(mState == PowerSourceService::PowerSourceState::Stopped)
                {
                    emit stopped();
                    emit stateChanged(PowerSourceService::PowerSourceState::Stopped);
                }
                else if(mState == PowerSourceService::PowerSourceState::Running)
                {
                    emit running();
                    emit stateChanged(PowerSourceService::PowerSourceState::Running);
                }
            }
            return true;
        }

    void AuxPowerUnit::setStateChangeTimeout()
    {
        if(mState == PowerSourceService::PowerSourceState::Starting)
        {
            setState(PowerSourceService::PowerSourceState::Stopped);
        }
        else if(mState == PowerSourceService::PowerSourceState::Stopping)
        {
            setState(PowerSourceService::PowerSourceState::Running);
        }
    }

    void AuxPowerUnit::sendSimulatedStarted()
    {
        setState(PowerSourceService::PowerSourceState::Running);
    }

    void AuxPowerUnit::sendSimulatedStopped()
    {
        setState(PowerSourceService::PowerSourceState::Stopped);
    }
}
