#include <QDebug>
#include "maingenerator.h"


namespace domain {

    const int cStartStopTimeout = 4000;

    MainGenerator::MainGenerator(QObject *parent) : QObject(parent),
        mStateChangeDuranceTimer(),
        mState(PowerSourceService::PowerSourceState::Stopped)
    {
        mStateChangeDuranceTimer.setSingleShot(true);
        connect(&mStateChangeDuranceTimer,&QTimer::timeout,this,&MainGenerator::setStateChangeTimeout);

        connect(this,&MainGenerator::simulatedRunning,this,&MainGenerator::sendSimulatedStarted);
        connect(this,&MainGenerator::simulatedStopped,this,&MainGenerator::sendSimulatedStopped);
    }

    void MainGenerator::tryToStart()
    {
        setState(PowerSourceService::PowerSourceState::Starting);
        mStateChangeDuranceTimer.start(cStartStopTimeout);
    }

    void MainGenerator::tryToStop()
    {
        setState(PowerSourceService::PowerSourceState::Stopping);
        mStateChangeDuranceTimer.start(cStartStopTimeout);
    }

    bool MainGenerator::isRunning()
    {
        bool lIsRunning = false;
        if(mState == PowerSourceService::PowerSourceState::Running)
        {
            lIsRunning = true;
        }
        return lIsRunning;
    }

    void MainGenerator::setState(PowerSourceService::PowerSourceState aState)
    {
        mState = aState;
        if(mState == PowerSourceService::PowerSourceState::Stopped)
        {
            mStateChangeDuranceTimer.stop();
            emit stopped();
        }
        else if(mState == PowerSourceService::PowerSourceState::Running)
        {
            mStateChangeDuranceTimer.stop();
            emit running();
        }
        emit stateChanged(mState);
    }

    bool MainGenerator::setUserCommand(PowerSourceService::UserCommand aCmd)
    {
        if(aCmd == PowerSourceService::UserCommand::Ok)
        {
            if(mState == PowerSourceService::PowerSourceState::Stopped)
            {
                tryToStart();
            }
            else if(mState == PowerSourceService::PowerSourceState::Running)
            {
                tryToStop();
            }
        }
        else if(aCmd == PowerSourceService::UserCommand::Cancel)
        {
            if(mState == PowerSourceService::PowerSourceState::Stopped)
            {
                emit stopped();
                emit stateChanged(PowerSourceService::PowerSourceState::Stopped);
            }
            else if(mState == PowerSourceService::PowerSourceState::Running)
            {
                emit running();
                emit stateChanged(PowerSourceService::PowerSourceState::Running);
            }
        }
        return true;
    }

    void MainGenerator::setStateChangeTimeout()
    {
        if(mState == PowerSourceService::PowerSourceState::Starting)
        {
            setState(PowerSourceService::PowerSourceState::Stopped);
        }
        else if(mState == PowerSourceService::PowerSourceState::Stopping)
        {
            setState(PowerSourceService::PowerSourceState::Running);
        }
    }

    void MainGenerator::sendSimulatedStarted()
    {
        setState(PowerSourceService::PowerSourceState::Running);
    }

    void MainGenerator::sendSimulatedStopped()
    {
        setState(PowerSourceService::PowerSourceState::Stopped);
    }
}
