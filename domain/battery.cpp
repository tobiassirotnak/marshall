#include <QDebug>
#include "battery.h"

namespace domain {

    Battery::Battery(QObject *parent) : QObject(parent),
        mRestEnduranceInMinutes(0),
        mMaxEnduranceInMinutes(100),
        mStateOfCharge(0)
    {

    }

    int Battery::getActualRestTime() const
    {
        return mRestEnduranceInMinutes;
    }

    void Battery::setRestTime(int aTime)
    {
        mRestEnduranceInMinutes = aTime;
        int lPercent = (mRestEnduranceInMinutes * 100) / mMaxEnduranceInMinutes;

        emit estimatedTimeChanged(mRestEnduranceInMinutes, lPercent);

        QVector<int> lSilentWatchTimeValues({60,120,180,240});
        emit silentWatchTimesChanged(lSilentWatchTimeValues);

        QVector<int> lPredictedTimeValues({0,30,90,150});
        emit predictedChargeTimeChanged(lPredictedTimeValues);
    }

    void Battery::setStateOfCharge(int aValue)
    {
        mStateOfCharge = aValue;
        emit stateOfChargeChanged();
        calculateRestTime();
    }

    int Battery::getStateOfCharge()
    {
        return mStateOfCharge;
    }

    void Battery::calculateRestTime()
    {
        int lRestTime = mStateOfCharge;
        setRestTime(lRestTime);
    }
}
