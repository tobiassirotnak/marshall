QT += quick

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

TRANSLATIONS = Translations/marshall_en.ts \
               Translations/marshall_de.ts \
               Translations/marshall_fr.ts \
               Translations/marshall_it.ts

SOURCES += \
        Controllers/DeviceListCtrl.cpp \
        Controllers/InfoBarCtrl.cpp \
        Controllers/MainCtrl.cpp \
        Controllers/PowerSourcesCtrl.cpp \
        Controllers/SimulatorCtrl.cpp \
        domain/auxpowerunit.cpp \
        domain/battery.cpp \
        domain/batterysimulator.cpp \
        domain/devicefsm.cpp \
        domain/energymgt.cpp \
        domain/maingenerator.cpp \
        domain/powersimulator.cpp \
        domain/powersourceservice.cpp \
        domain/simulatorservice.cpp \
        main.cpp \
        domain/deviceservice.cpp \
        domain/device.cpp \
        domain/devicesimulator.cpp \
        data/configurationservice.cpp

RESOURCES += qml.qrc \
    images.qrc \
    sounds.qrc \
    translations.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    Controllers/DeviceListCtrl.h \
    Controllers/InfoBarCtrl.h \
    Controllers/MainCtrl.h \
    Controllers/PowerSourcesCtrl.h \
    Controllers/SimulatorCtrl.h \
    domain/auxpowerunit.h \
    domain/battery.h \
    domain/batterysimulator.h \
    domain/devicefsm.h \
    domain/deviceservice.h \
    domain/device.h \
    domain/devicesimulator.h \
    data/configurationservice.h \
    domain/energymgt.h \
    domain/maingenerator.h \
    domain/powersimulator.h \
    domain/powersourceservice.h \
    domain/simulatorservice.h

OTHER_FILES += Devices.json
