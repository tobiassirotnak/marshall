#include <QDebug>
#include "SimulatorCtrl.h"
#include "domain/simulatorservice.h"

SimulatorCtrl::SimulatorCtrl(QQmlContext *rootContext) :
    mSimulatorService(nullptr)
{
    rootContext->setContextProperty("simulatorCtrl", this);
}

void SimulatorCtrl::initializeComponents()
{
    mSimulatorService = new domain::SimulatorService(this);
}

void SimulatorCtrl::setDeviceService(domain::DeviceService *aService)
{    
    mSimulatorService->setDeviceService(aService);
}

void SimulatorCtrl::setPowerSourceService(domain::PowerSourceService *aService)
{
    mSimulatorService->setPowerSourceService(aService);
}

void SimulatorCtrl::startMainGenerator()
{
    mSimulatorService->startMainGenerator();
}

void SimulatorCtrl::stopMainGenerator()
{
    mSimulatorService->stopMainGenerator();
}

void SimulatorCtrl::startApuGenerator()
{
    mSimulatorService->startAuxPowerUnit();
}

void SimulatorCtrl::stopApuGenerator()
{
    mSimulatorService->stopAuxPowerUnit();
}

void SimulatorCtrl::setAnimated(bool aMustAnimate)
{
    mSimulatorService->startActualValueSimulation(aMustAnimate);
}

void SimulatorCtrl::setBatterySoC(int aValue)
{
    if(mSimulatorService != nullptr)
    {
        mSimulatorService->setBatterySoC(aValue);
    }
}

void SimulatorCtrl::startBatteryRestTimeSimulation(bool aStart)
{
    mSimulatorService->startBatteryRestTimeSimulation(aStart);
}

