#ifndef MAINCTRL_H
#define MAINCTRL_H

#include <QObject>
#include <QQmlContext>
#include "PowerSourcesCtrl.h"
#include "DeviceListCtrl.h"

namespace domain {
    class DeviceService;
    class PowerSourceService;
    class EnergyMgt;
}

namespace data {
    class ConfigurationService;
}

class MainCtrl : public QObject
{
    Q_OBJECT
public:
    explicit MainCtrl(QQmlContext *rootContext);

    void initializeComponents();
    void connectServiceComponents();
    void startupSystem();

    domain::DeviceService* getDeviceService();
    domain::PowerSourceService* getPowerSourceService();

private:
    PowerSourcesCtrl *_powerSourcesCtrl = nullptr;
    DeviceListCtrl *_deviceListCtrl = nullptr;
    domain::DeviceService* mDeviceService;
    domain::PowerSourceService* mPowerSourceService;
    domain::EnergyMgt* mEnergyMgt;
    data::ConfigurationService* mConfigurationService;

};

#endif // MAINCTRL_H
