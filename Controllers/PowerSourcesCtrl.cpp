#include "PowerSourcesCtrl.h"
#include <QDebug>
#include <QTimer>
#include <QRandomGenerator>
#include "InfoBarCtrl.h"

#include "domain/powersourceservice.h"

PowerSourcesCtrl::PowerSourcesCtrl(QObject *parent) : QObject(parent),
    mPowerSourceService(nullptr)
{

}

void PowerSourcesCtrl::setPowerSourceService(domain::PowerSourceService *aService)
{
    mPowerSourceService = aService;
}

void PowerSourcesCtrl::tryToChangeState(bool isMainGen, int state)
{
    bool lMustSendCommand = false;
    domain::PowerSourceService::UserCommand lCommand = domain::PowerSourceService::UserCommand::Undefined;

    if(isMainGen)
    {
        switch(state)
        {
            case 0:
            {
                lCommand = domain::PowerSourceService::UserCommand::Turn_Off;
                emit setMainGeneratorState( GState::ASK_FOR_SHUTTING_DOWN );
                break;
            }
            case 3:
            {
                lCommand = domain::PowerSourceService::UserCommand::Turn_On;
                emit setMainGeneratorState( GState::ASK_FOR_RUNNING );
                break;
            }
            case 4:
            case 5:
            {
                lCommand = domain::PowerSourceService::UserCommand::Ok;
                lMustSendCommand = true;
                break;
            }
            case 6:
            {
                lCommand = domain::PowerSourceService::UserCommand::Cancel;
                lMustSendCommand = true;
                break;
            }
            default:
            {
                break;
            }
        }
    }
    else
    {
        switch(state)
        {
            case 0:
            {
                lCommand = domain::PowerSourceService::UserCommand::Turn_Off;
                emit setApuGeneratorState( GState::ASK_FOR_SHUTTING_DOWN );
                break;
            }
            case 3:
            {
                lCommand = domain::PowerSourceService::UserCommand::Turn_On;
                emit setApuGeneratorState( GState::ASK_FOR_RUNNING );
                break;
            }
            case 4:
            case 5:
            {
                lCommand = domain::PowerSourceService::UserCommand::Ok;
                lMustSendCommand = true;
                break;
            }
            case 6:
            {
                lCommand = domain::PowerSourceService::UserCommand::Cancel;
                lMustSendCommand = true;
                break;
            }
            default:
            {
                break;
            }
        }
    }

    if(lMustSendCommand && (mPowerSourceService != nullptr))
    {
        mPowerSourceService->setPowerSourceCommand(isMainGen, lCommand);
    }
}

void PowerSourcesCtrl::setMainGeneratorStarted()
{
    setGeneratorState(static_cast<int>(domain::PowerSourceService::PowerSourceState::Running));
}

void PowerSourcesCtrl::setMainGeneratorStopped()
{
    setGeneratorState(domain::PowerSourceService::PowerSourceState::Stopped);
}

void PowerSourcesCtrl::setGeneratorState(int aState)
{
    switch(aState)
    {
        case static_cast<int>(domain::PowerSourceService::PowerSourceState::Stopped):
        {
            emit setMainGeneratorState( GState::SHUTDOWN );
            break;
        }
        case static_cast<int>(domain::PowerSourceService::PowerSourceState::Running):
        {
            emit setMainGeneratorState( GState::RUNNING );
            break;
        }
        case static_cast<int>(domain::PowerSourceService::PowerSourceState::Starting):
        {
            emit setMainGeneratorState( GState::WAIT_FOR_RUNNING );
            break;
        }
        case static_cast<int>(domain::PowerSourceService::PowerSourceState::Stopping):
        {
            emit setMainGeneratorState( GState::WAIT_FOR_SHUTTING_DOWN );
            break;
        }
    }
}

void PowerSourcesCtrl::setAPUStarted()
{
    emit setApuGeneratorState( GState::RUNNING );
}

void PowerSourcesCtrl::setAPUStopped()
{
    emit setApuGeneratorState( GState::SHUTDOWN );
}

void PowerSourcesCtrl::setApuState(int aState)
{
    switch(aState)
    {
        case static_cast<int>(domain::PowerSourceService::PowerSourceState::Stopped):
        {
            emit setApuGeneratorState( GState::SHUTDOWN );
            break;
        }
        case static_cast<int>(domain::PowerSourceService::PowerSourceState::Running):
        {
            emit setApuGeneratorState( GState::RUNNING );
            break;
        }
        case static_cast<int>(domain::PowerSourceService::PowerSourceState::Starting):
        {
            emit setApuGeneratorState( GState::WAIT_FOR_RUNNING );
            break;
        }
        case static_cast<int>(domain::PowerSourceService::PowerSourceState::Stopping):
        {
            emit setApuGeneratorState( GState::WAIT_FOR_SHUTTING_DOWN );
            break;
        }
    }
}

void PowerSourcesCtrl::setBatteryEstimatedTime(int minutes, int percent)
{
    emit showBatteryRestTime(minutes, percent);
}

void PowerSourcesCtrl::setSilentWatchTimes(QVector<int> aTimeList)
{
    emit showSilentWatchTimes(aTimeList);
}

void PowerSourcesCtrl::setPredictedTime(QVector<int> aTimeList)
{
    emit showPredictedTime(aTimeList);
}

