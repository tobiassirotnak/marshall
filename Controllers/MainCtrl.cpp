#include "MainCtrl.h"
#include <QQmlEngine>
#include "InfoBarCtrl.h"

#include "domain/simulatorservice.h"
#include "domain/deviceservice.h"
#include "domain/powersourceservice.h"
#include "domain/energymgt.h"
#include "domain/maingenerator.h"
#include "domain/auxpowerunit.h"
#include "domain/battery.h"
#include "data/configurationservice.h"

MainCtrl::MainCtrl(QQmlContext *rootContext):
    mDeviceService(nullptr),
    mPowerSourceService(nullptr),
    mEnergyMgt(nullptr),
    mConfigurationService(nullptr)
{
    rootContext->setContextProperty("infoBarCtrl", InfoBarCtrl::info());

    _powerSourcesCtrl = new PowerSourcesCtrl(this);
    rootContext->setContextProperty("powerSourcesCtrl", _powerSourcesCtrl);
    qRegisterMetaType<GState>("GState");
    qmlRegisterUncreatableType<GeneratorState>("Generator.State", 1, 0, "GState", "UncreatableType!");

    _deviceListCtrl = new DeviceListCtrl(this);
    rootContext->setContextProperty("deviceListCtrl", _deviceListCtrl);
}

void MainCtrl::initializeComponents()
{
    domain::MainGenerator* lMainGenerator = new domain::MainGenerator(this);
    domain::AuxPowerUnit* lAuxPowerUnit = new domain::AuxPowerUnit(this);
    domain::Battery* lBattery = new domain::Battery(this);

    mConfigurationService = new data::ConfigurationService(this);
    mDeviceService = new domain::DeviceService(this);
    mDeviceService->setConfigurationService(mConfigurationService);
    mDeviceService->loadDevices();
    mDeviceService->loadProfiles();
    _deviceListCtrl->setDeviceService(mDeviceService);    

    mPowerSourceService = new domain::PowerSourceService(this);
    mPowerSourceService->setMainGenerator(lMainGenerator);
    mPowerSourceService->setAuxPowerUnit(lAuxPowerUnit);
    mPowerSourceService->setBattery(lBattery);
    mPowerSourceService->connectComponents();
    _powerSourcesCtrl->setPowerSourceService(mPowerSourceService);

    mEnergyMgt = new domain::EnergyMgt(this);
    mEnergyMgt->setMainGenerator(lMainGenerator);
    mEnergyMgt->setAuxPowerUnit(lAuxPowerUnit);
    mEnergyMgt->setBattery(lBattery);
    connect(lBattery,&domain::Battery::stateOfChargeChanged,mEnergyMgt,&domain::EnergyMgt::setBatteryStateOfCharge);
    connect(mPowerSourceService, &domain::PowerSourceService::mainGeneratorStarted, mEnergyMgt, &domain::EnergyMgt::startMainGenerator);
    connect(mPowerSourceService, &domain::PowerSourceService::mainGeneratorStopped, mEnergyMgt, &domain::EnergyMgt::stopMainGenerator);
    connect(mPowerSourceService, &domain::PowerSourceService::apuGeneratorStarted, mEnergyMgt, &domain::EnergyMgt::startApuGenerator);
    connect(mPowerSourceService, &domain::PowerSourceService::apuGeneratorStopped, mEnergyMgt, &domain::EnergyMgt::stopApuGenerator);
    connect(mEnergyMgt,&domain::EnergyMgt::batteryStateOfChargeChanged,mDeviceService,&domain::DeviceService::setBatteryStateOfCharge);
    connect(mDeviceService, &domain::DeviceService::profileChanged, mEnergyMgt, &domain::EnergyMgt::setBatteryStateOfCharge);
}

void MainCtrl::connectServiceComponents()
{
    // connection from domain::DeviceService to GUI
    connect(mDeviceService, &domain::DeviceService::actualValueChanged, _deviceListCtrl, &DeviceListCtrl::setactualPower); // _deviceListCtrl->setMaxPower(2, 1000, 0);
    connect(mDeviceService, &domain::DeviceService::midValueChanged, _deviceListCtrl, &DeviceListCtrl::setMidPower); // _deviceListCtrl->setMidPower(2, 800);
    connect(mDeviceService, &domain::DeviceService::deviceStateChanged, _deviceListCtrl, &DeviceListCtrl::setState);
    connect(mDeviceService, &domain::DeviceService::switchToProfile, _deviceListCtrl, &DeviceListCtrl::setProfile); // _deviceListCtrl->setProfile(0);
    //void restTimeChanged(int,QString); FIXME: Please, give me duration in minutes (int) instead of english string (QString)
    connect(mDeviceService, &domain::DeviceService::restTimeChanged, _deviceListCtrl, &DeviceListCtrl::setRestTime);    

    connect(mPowerSourceService, &domain::PowerSourceService::mainGeneratorStarted, _powerSourcesCtrl, &PowerSourcesCtrl::setMainGeneratorStarted);
    connect(mPowerSourceService, &domain::PowerSourceService::mainGeneratorStopped, _powerSourcesCtrl, &PowerSourcesCtrl::setMainGeneratorStopped);
    connect(mPowerSourceService, &domain::PowerSourceService::mainGeneratorStateChanged, _powerSourcesCtrl, &PowerSourcesCtrl::setGeneratorState);
    connect(mPowerSourceService, &domain::PowerSourceService::apuGeneratorStarted, _powerSourcesCtrl, &PowerSourcesCtrl::setAPUStarted);
    connect(mPowerSourceService, &domain::PowerSourceService::apuGeneratorStopped, _powerSourcesCtrl, &PowerSourcesCtrl::setAPUStopped);
    connect(mPowerSourceService, &domain::PowerSourceService::apuStateChanged, _powerSourcesCtrl, &PowerSourcesCtrl::setApuState);
    connect(mPowerSourceService, &domain::PowerSourceService::batteryEstimatedTimeChanged, _powerSourcesCtrl, &PowerSourcesCtrl::setBatteryEstimatedTime);
    connect(mPowerSourceService, &domain::PowerSourceService::silentWatchTimesChanged, _powerSourcesCtrl, &PowerSourcesCtrl::setSilentWatchTimes);
    connect(mPowerSourceService, &domain::PowerSourceService::predictedTimeChanged, _powerSourcesCtrl, &PowerSourcesCtrl::setPredictedTime);

    // connection from GUI to domain::DeviceService
    //connect(_deviceListCtrl, &DeviceListCtrl::beginToChangeState, mDeviceService, &domain::DeviceService::TODO); // beginToChangeState(int profileId, domain::DeviceService::CommandSubState)
}

void MainCtrl::startupSystem()
{
    mDeviceService->startupSystem();
}

domain::DeviceService *MainCtrl::getDeviceService()
{
    return mDeviceService;
}

domain::PowerSourceService *MainCtrl::getPowerSourceService()
{
    return mPowerSourceService;
}
