#ifndef SIMULATORCTRL_H
#define SIMULATORCTRL_H

#include <QObject>
#include <QQmlContext>

namespace domain {
    class DeviceService;
    class PowerSourceService;
    class SimulatorService;
}

class SimulatorCtrl : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool animate WRITE setAnimated)

public:
    explicit SimulatorCtrl(QQmlContext *rootContext);

    void initializeComponents();
    void setDeviceService(domain::DeviceService* aService);
    void setPowerSourceService(domain::PowerSourceService* aService);

    Q_INVOKABLE void startMainGenerator();
    Q_INVOKABLE void stopMainGenerator();
    Q_INVOKABLE void startApuGenerator();
    Q_INVOKABLE void stopApuGenerator();
    Q_INVOKABLE void setAnimated(bool aMustAnimate);
    Q_INVOKABLE void setBatterySoC(int aValue);
    Q_INVOKABLE void startBatteryRestTimeSimulation(bool aStart);

private:
    domain::SimulatorService* mSimulatorService;

signals:

};

#endif // SIMULATORCTRL_H
