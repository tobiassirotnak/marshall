#ifndef INFOBARCTRL_H
#define INFOBARCTRL_H

#include <QObject>
#include <QTranslator>

class InfoBarCtrl : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString emptyString READ emptyString NOTIFY languageChanged)
    Q_PROPERTY(QString language READ language NOTIFY languageChanged)

public:
    explicit InfoBarCtrl(QObject *parent = nullptr);
    static InfoBarCtrl *info();
    static void destroy();
    QString emptyString() { return ""; }
    Q_INVOKABLE void setLanguage(const QString &language);
    const QString language() { return _currentLanguage; }

signals:
    // show 'text' in qml for the 'duration' time [seconds]. 0 means show it until next text comes
    void show(const QString &text, int duration = 0);
    void languageChanged();

private:
    static QScopedPointer<InfoBarCtrl> sharedInstance;
    QTranslator *_translator = nullptr;
    QString _currentLanguage = "en";
};

#endif // INFOBARCTRL_H
