#include "InfoBarCtrl.h"
#include <QCoreApplication>
#include <QLocale>
#include <QDebug>

QScopedPointer<InfoBarCtrl> InfoBarCtrl::sharedInstance;

// static function reached from any part of the code, just add: #include "InfoBarCtrl.h"
InfoBarCtrl *InfoBarCtrl::info()
{
    if(sharedInstance == nullptr) {
        QScopedPointer<InfoBarCtrl> ibc(new InfoBarCtrl);
        sharedInstance.swap(ibc);
    }
    return sharedInstance.data();
}

void InfoBarCtrl::destroy()
{
    QScopedPointer<InfoBarCtrl> null;
    sharedInstance.swap(null);
}

InfoBarCtrl::InfoBarCtrl(QObject *parent) : QObject(parent)
{
    setLanguage(_currentLanguage);
}

void InfoBarCtrl::setLanguage(const QString &language)
{
    qDebug() << "setLanguage: " << language;
    _currentLanguage = language;
    // load the new translator
    if(_translator)
        qApp->removeTranslator(_translator);
    _translator = new QTranslator(this);
    _translator->load(QString("marshall_%1.qm").arg(language), ":/Translations");
    qApp->installTranslator(_translator);
    emit languageChanged();
}

