#ifndef DEVICELISTCTRL_H
#define DEVICELISTCTRL_H

#include <QObject>
#include <QStandardItemModel>
#include "domain/deviceservice.h"

namespace domain {
    class DeviceService;
}

class Device
{
    Q_GADGET // lighter version of Q_OBJECT is enough to use Q_PROPERTY and Q_INVOKABLE
    Q_PROPERTY(QString name READ name)
    Q_PROPERTY(int actualPower READ actualPower)
    Q_PROPERTY(int remainingMinutes READ remainingMinutes)
    Q_PROPERTY(int actualPowerBarLength READ actualPowerBarLength)
    Q_PROPERTY(int midPower READ midPower)
    Q_PROPERTY(QString state READ state)
public:
    Device(const QString& name, const QString &state) : _name(name), _state(state) { }
    Device() = default;
    Device(const Device& otherDevice) = default;
    Device& operator=(const Device& otherDevice) = default;
    const QString& name() const { return _name; }
    int actualPower() const { return _actualPower; }
    int remainingMinutes() const { return _remainingMinutes; }
    void setActualPower(int actualPower) { _actualPower = actualPower; }
    void setRemainingMinutes(int remainingMinutes) { _remainingMinutes = remainingMinutes; }
    int actualPowerBarLength() const { return _actualPowerBarLength; }
    void setActualPowerBarLength(int actualPowerBarLength) { _actualPowerBarLength = actualPowerBarLength; }
    int midPower() const { return _midPower; }
    void setMidPower(int midPower) { _midPower = midPower; }
    QString state() const { return _state; }
    void setState(const QString &state) { _state = state; }

private:
    QString _name = QString();
    int _actualPower = 0;
    int _actualPowerBarLength = 0;
    int _remainingMinutes = 0;
    int _midPower = 0;
    QString _state = "Off";
};

class DeviceListCtrl : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel* model READ model CONSTANT)
    Q_DISABLE_COPY(DeviceListCtrl)
public:
    explicit DeviceListCtrl(QObject *parent = nullptr);
    Q_INVOKABLE void loadDevices(int profileId);
    Q_INVOKABLE void tryToChangeState(int deviceId, const QString &state);
    void addDevice(const QString &name, const QString &state);
    QAbstractItemModel* model() const { return _deviceListModel; }

    void setDeviceService(domain::DeviceService* aService);
    void changeProfileType(int profileId);

    // edit model data
public slots:
    void setactualPower(int id, int calcPower, int realPower);
    void setMidPower(int id, int midPower);
    void setRestTime(int id, int remainingMinutes);
    void setState(int id, domain::DeviceDescriptor descriptor);

signals:
    void setProfile(int profileId);
    void beginToChangeState(int profileId, int suggestedState);

private:
    QAbstractItemModel* _deviceListModel;
    domain::DeviceService* mDeviceService;
};

#endif // DEVICELISTCTRL_H
