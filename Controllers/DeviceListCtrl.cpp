#include "DeviceListCtrl.h"
#include <QDebug>
#include <QDir>

DeviceListCtrl::DeviceListCtrl(QObject *parent) : QObject(parent),
    mDeviceService(nullptr)
{
    // setup device model
    _deviceListModel = new QStandardItemModel(this);
    _deviceListModel->insertColumn(0);
}

// create new device and add it to model
void DeviceListCtrl::addDevice(const QString &name, const QString &state)
{
    const int newRow = _deviceListModel->rowCount();
    const Device newDevice(name, state);
    _deviceListModel->insertRow(newRow);
    _deviceListModel->setData(_deviceListModel->index(newRow, 0), QVariant::fromValue(newDevice), Qt::EditRole);
}

// The calcPower is the actual value in percent (range: 0-100%)
// The realpower is the actual value in W
void DeviceListCtrl::setactualPower(int id, int calcPower, int realPower)
{
    //Q_UNUSED(realPower)
    int row = id-1;
    auto deviceMap = _deviceListModel->itemData(_deviceListModel->index(row, 0));
    if(deviceMap.size()) {
        Device device = deviceMap.first().value<Device>();
        //qDebug() << "Change for device " << device.name() << ": actualPower in W: " << realPower << ", length of the bar in %:" << calcPower;
        device.setActualPower(realPower);
        device.setActualPowerBarLength(calcPower);
        _deviceListModel->setData(_deviceListModel->index(row, 0), QVariant::fromValue(device), Qt::EditRole);
    }
}

void DeviceListCtrl::setMidPower(int id, int midPower)
{
    int row = id-1;
    auto deviceMap = _deviceListModel->itemData(_deviceListModel->index(row, 0));
    if(deviceMap.size()) {
        Device device = deviceMap.first().value<Device>();
        //qDebug() << "Change for device " << device.name() << ": midPower: " << device.midPower() << "=>" << midPower;
        device.setMidPower(midPower);
        _deviceListModel->setData(_deviceListModel->index(row, 0), QVariant::fromValue(device), Qt::EditRole);
    }
}

void DeviceListCtrl::setRestTime(int id, int remainingMinutes)
{
    int row = id-1;
    auto deviceMap = _deviceListModel->itemData(_deviceListModel->index(row, 0));
    if(deviceMap.size()) {
        Device device = deviceMap.first().value<Device>();
        //qDebug() << "Change for device " << device.name() << ": remainingMinutes: " << remainingMinutes;
        device.setRemainingMinutes(remainingMinutes);
        _deviceListModel->setData(_deviceListModel->index(row, 0), QVariant::fromValue(device), Qt::EditRole);
    }
}

void DeviceListCtrl::setState(int id, domain::DeviceDescriptor descriptor)
{
    int row = id-1;
    auto deviceMap = _deviceListModel->itemData(_deviceListModel->index(row, 0));
    if(deviceMap.size()) {
        Device device = deviceMap.first().value<Device>();
        QString state = "Off";
        switch(descriptor.CommandState) {
        case static_cast<int>(domain::DeviceService::CommandSubState::Off): {
            state = "Off"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::Running): {
            state = "Running"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::RunningLow): {
            state = "Running_Power_Low"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::AskForTurningOff): {
            state = "AskForTurningOff"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::AskForTurningOn): {
            state = "AskForTurningOn"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::Disabled): {
            state = "Disabled"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::AskForSource): {
            state = "AskForSource"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::Override): {
            state = "Override"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::AskForOverride): {
            state = "AskForOverride"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::AskForReset): {
            state = "AskForReset"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::AskForAction): {
            state = "AskForAction"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::WaitForRunning): {
            state = "WaitForRunning"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::WaitForOff): {
            state = "WaitForOff"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::WaitForOverride): {
            state = "WaitForOverride"; break;
        }
        case static_cast<int>(domain::DeviceService::CommandSubState::WaitForReset): {
            state = "WaitForReset"; break;
        }
        }
        qDebug() << "Change for device " << device.name() << ": state: " << device.state() << "=>" << state;
        device.setState(state);
        _deviceListModel->setData(_deviceListModel->index(row, 0), QVariant::fromValue(device), Qt::EditRole);
    }
}

// suggested state change comes from GUI and is emitted to Domain layer
void DeviceListCtrl::tryToChangeState(int deviceId, const QString &state)
{
    qDebug() << "DeviceListCtrl::tryToChangeState" << deviceId << state;

    domain::DeviceService::UserCommand lCommand = domain::DeviceService::UserCommand::Undefined;

    if(state == "Turn_Off")
        lCommand = domain::DeviceService::UserCommand::Turn_Off;
    else if(state == "Turn_On")
        lCommand = domain::DeviceService::UserCommand::Turn_On;
    else if (state == "OK_AskForTurningOn" || state == "OK_AskForTurningOff" || state == "OK_AskForOverride" || state == "OK_AskForReset")
        lCommand = domain::DeviceService::UserCommand::Ok;
    else if(state == "Cancel")
        lCommand = domain::DeviceService::UserCommand::Cancel;
    else if(state == "Override")
        lCommand = domain::DeviceService::UserCommand::Override;
    else if(state == "Reset")
        lCommand = domain::DeviceService::UserCommand::Reset;
    else if(state == "Action_Required")
        lCommand = domain::DeviceService::UserCommand::Action_Required;

    if(mDeviceService != nullptr)
    {
        mDeviceService->setDeviceCommand(deviceId, lCommand);
    }
}

void DeviceListCtrl::setDeviceService(domain::DeviceService *aService)
{
    mDeviceService = aService;
}

// read device data for selected profile
void DeviceListCtrl::loadDevices(int profileId)
{
    QVector<domain::DeviceDescriptor> lDeviceDescrList;

    if(mDeviceService != nullptr)
    {
        changeProfileType(profileId);

        domain::DeviceService::DeviceProfile lProfileType = static_cast<domain::DeviceService::DeviceProfile>(profileId);
        lDeviceDescrList = mDeviceService->getDeviceDescr(lProfileType);
    }

    // fillup for the first time
    if(_deviceListModel->rowCount() != lDeviceDescrList.count())
    {
        for (int i = 0; i < lDeviceDescrList.size(); ++i)
        {
            QString state = "Off";
            addDevice(lDeviceDescrList.at(i).Name, state);
        }
    }

    // Update existing list
    for (int i = 0; i < lDeviceDescrList.size(); ++i)
    {
        setState(i, lDeviceDescrList.at(i));
    }
}

void DeviceListCtrl::changeProfileType(int profileId)
{
    qDebug() << "DeviceListCtrl::changeProfileType" << profileId;
    mDeviceService->setProfile(profileId);
}
