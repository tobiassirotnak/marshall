#ifndef POWERSOURCESCTRL_H
#define POWERSOURCESCTRL_H

#include <QObject>

namespace domain {
    class PowerSourceService;
}

class GeneratorState
{
    Q_GADGET
public:
    enum State {
        SHUTDOWN,
        ASK_FOR_RUNNING,
        WAIT_FOR_RUNNING,
        RUNNING,
        ASK_FOR_SHUTTING_DOWN,
        WAIT_FOR_SHUTTING_DOWN
    };
    Q_ENUM(State)

private:
    explicit GeneratorState();
};

typedef GeneratorState::State GState;

class PowerSourcesCtrl : public QObject
{
    Q_OBJECT

public:
    explicit PowerSourcesCtrl(QObject *parent = nullptr);

    void setPowerSourceService(domain::PowerSourceService* aService);
    Q_INVOKABLE void tryToChangeState(bool isMainGen, int state);

public slots:
    void setMainGeneratorStarted();
    void setMainGeneratorStopped();
    void setGeneratorState(int aState);
    void setAPUStarted();
    void setAPUStopped();
    void setApuState(int aState);
    void setBatteryEstimatedTime(int minutes, int percent);
    void setSilentWatchTimes(QVector<int> aTimeList);
    void setPredictedTime(QVector<int> aTimeList);

signals:
    void setMainGeneratorState(GState state);
    void setApuGeneratorState(GState state);
    void showBatteryRestTime(int minutes, int percent);
    void showSilentWatchTimes(QVector<int> aTimeList);
    void showPredictedTime(QVector<int> aTimeList);

private:
    domain::PowerSourceService* mPowerSourceService;
};

#endif // POWERSOURCESCTRL_H
