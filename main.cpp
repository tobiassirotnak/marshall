#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QScreen>
#include <QQmlContext>
#include "Controllers/MainCtrl.h"
#include "Controllers/SimulatorCtrl.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    // create GUI in QML
    QQmlApplicationEngine engine;

    QQmlContext *context = engine.rootContext();

    // Main app controller
    MainCtrl mainCtrl(context);
    mainCtrl.initializeComponents();

    // Simulator app controller
    SimulatorCtrl simulatorCtrl(context);
    simulatorCtrl.initializeComponents();
    simulatorCtrl.setDeviceService(mainCtrl.getDeviceService());
    simulatorCtrl.setPowerSourceService(mainCtrl.getPowerSourceService());
    //simulatorCtrl.setAnimated(true);
    //simulatorCtrl.startBatteryRestTimeSimulation(true);

    // Get screen geometry, and set the GUI for this size.
    // The size should be 1920x1200 (1.6:1)
    //QRect geometry = QGuiApplication::primaryScreen()->availableGeometry();
    //context->setContextProperty(QStringLiteral("_screenWidth"), geometry.width());
    //context->setContextProperty(QStringLiteral("_screenHeight"), geometry.height());
    // used just during developing
    context->setContextProperty(QStringLiteral("_screenWidth"), 1920/2);
    context->setContextProperty(QStringLiteral("_screenHeight"), 1200/2);
    // TODO: UNCOMMENT FOR RUNNING ON TABLET IN FULLSCREEN MODE
    //context->setContextProperty(QStringLiteral("_screenWidth"), 1920);
    //context->setContextProperty(QStringLiteral("_screenHeight"), 1200);

    const QUrl url(QStringLiteral("qrc:/Qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    mainCtrl.connectServiceComponents();
    mainCtrl.startupSystem();

    return app.exec();
}
